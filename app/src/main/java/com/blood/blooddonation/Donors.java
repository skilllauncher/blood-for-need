package com.blood.blooddonation;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class Donors extends AppCompatActivity {

    // This class is used for testing purpose only and not displayed in the app. This class is used to display all the donors in the database.

    private ListView listview;
    private List<Attributes> AttributesList;
    JSONArray result;
    Location location;
    private LocationManager locationManager;
    int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private String google_map_link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donors);

        Toolbar toolbar = findViewById(R.id.toolbar_donor);
        toolbar.setBackgroundColor(getResources().getColor(R.color.white));

        // Initialization of the list view.
        listview = findViewById(R.id.listview1);

        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setHomeButtonEnabled(true);
        }

        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.left);
        toolbar.setNavigationIcon(drawable);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        AttributesList = new ArrayList<>();

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        //Used for checking the location permission
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            return;
        }

        //Used for the fetching the last known location
        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        double lat = location.getLatitude();
        double lon = location.getLongitude();

        //Used for generating a map link for navigation purpose
        google_map_link = "https://www.google.com/maps?q=" + lat + "," + lon;

        Log.i("JSON", "The distance is " + String.valueOf(distance(lat, lon, 17.6868, 83.2185)));
        displayAllPosts();

    }

    //Used for displaying all the donors
    private void displayAllPosts() {
        String displayurl = "https://bloodtransfer.herokuapp.com/index.php/data/searchall";

        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();

        client.post(displayurl, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i("JSON", "JSON is " + response.toString());
                Log.i("JSON", "Status  code" + statusCode);
                Log.i("JSON", response.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                Log.i("JSON", "Status code" + statusCode);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i("JSON", "Status  code" + statusCode);
                Log.i("JSON", responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                Log.i("JSON", response.toString());
                Log.i("JSON", "Length " + response.length());
                if (response.length() > 0) {
                    result = response;
                    CustomAdapter adapter = new CustomAdapter();
                    listview.setAdapter(adapter);
                } else {
                    Log.i("JSON", "No donors");
                }

            }
        });
    }

    //Used for appending the data on the cardview
    class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return result.length();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.cardview, null);

            TextView name = convertView.findViewById(R.id.name_card);
            TextView place = convertView.findViewById(R.id.place);
            TextView blood = convertView.findViewById(R.id.blood_group_card);
            TextView last_d = convertView.findViewById(R.id.last_donated_card);
            TextView image = convertView.findViewById(R.id.photo);
            Button contact = convertView.findViewById(R.id.help);

            try {
                final JSONObject object = result.getJSONObject(position);
                name.setText(object.getString("name"));
                place.setText(object.getString("city"));
                blood.setText(object.getString("bloodgroup"));
                last_d.setText("Last donated on " + object.getString("last_donated"));
                contact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), RequestActivity.class);
                        try {
                            intent.putExtra("don_name", object.getString("name"));
                            intent.putExtra("number", object.getString("mobile"));
                            //intent.putExtra("id",object.getString("id"));
                            intent.putExtra("bloodgroup", object.getString("bloodgroup"));
                            intent.putExtra("city", object.getString("city"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(intent);
                    }
                });
                String fl = object.getString("name").substring(0, 1);
                image.setText(fl);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return convertView;
        }
    }


    //Used for finding the distance between 2 latlng
    public float distance(double lat_a, double lng_a, double lat_b, double lng_b) {
        Log.i("JSON", "Distance " + lat_a + " " + lng_a + " " + lat_b + " " + lng_b);
        double earthRadius = 3958.75;
        double latDiff = Math.toRadians(lat_b - lat_a);
        double lngDiff = Math.toRadians(lng_b - lng_a);
        double a = Math.sin(latDiff / 2) * Math.sin(latDiff / 2) +
                Math.cos(Math.toRadians(lat_a)) * Math.cos(Math.toRadians(lat_b)) *
                        Math.sin(lngDiff / 2) * Math.sin(lngDiff / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = earthRadius * c;

        int meterConversion = 1609;

        return (float) (distance * meterConversion) / 1000;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,HomeScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }
}

package com.blood.blooddonation;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;


public class Profile extends AppCompatActivity {

    private String email = "fetching..";
    DatePickerDialog.OnDateSetListener dateSetListener;
    ImageView male, female;
    private int i;
    private boolean ma = false, fe = false;
    private String gender;
    EditText pname;
    TextView date;
    EditText contact;
    private String place_string, cityName;
    private String address_string;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        ImageButton back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });

        contact = findViewById(R.id.cnt);
        pname = findViewById(R.id.pname);
        male = findViewById(R.id.imageView);
        female = findViewById(R.id.imageView2);


        getEmail();

        profile();

        male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (i == 1) {
                    male.setImageResource(R.drawable.cman);
                    i = 0;
                    ma = true;
                    gender = "male";
                } else if (i == 0) {
                    male.setImageResource(R.drawable.cman);
                    ma = true;
                    female.setImageResource(R.drawable.woman);
                }

            }
        });


        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (i == 1) {
                    female.setImageResource(R.drawable.cwomen);
                    fe = true;
                    gender = "female";
                    i = 0;
                } else if (i == 0) {
                    female.setImageResource(R.drawable.cwomen);
                    fe = true;
                    male.setImageResource(R.drawable.man1);

                }
            }
        });

        date = findViewById(R.id.date);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int date = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(Profile.this, android.R.style.Theme_Holo_Dialog_NoActionBar_MinWidth, dateSetListener, year, month, date);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;

                String dat = year + "/" + month + "/" + dayOfMonth;
                date.setText(dat);
            }
        };


        Button submit = findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfile(email, gender, pname.getText().toString(), contact.getText().toString(), cityName, address_string);
            }
        });


        //Autocomplete Place Retrieval using Google Place API
        Places.initialize(getApplicationContext(), "AIzaSyC1u7yPHGjEvVUPFpCo6P_aBOrBNDlJKIk");
        // Initialize the AutocompleteSupportFragment.
        final AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        // Specify the types of place data to return.
        assert autocompleteFragment != null;
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.LAT_LNG, Place.Field.NAME, Place.Field.ADDRESS));

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i("Add", "Place: " + place.getName() + ", " + place.getId());
                place_string = place.getName();
                address_string = place.getAddress();

                LatLng latlng = place.getLatLng();
                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                try {
                    List<Address> addresses = geocoder.getFromLocation(latlng.latitude, latlng.longitude, 1);
                    cityName = addresses.get(0).getAddressLine(0);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("Add", "An error occurred: " + status);
            }
        });

    }

    private void updateProfile(String email, String gender, String pname, String contact, String cityName, String address_string) {
        String request_url = "https://bloodtransfer.herokuapp.com/index.php/data/reciever";

        RequestParams params = new RequestParams();
        params.put("email", email);
        params.put("gender", gender);
        params.put("name", pname);
        params.put("mobile", contact);
        params.put("city", cityName);
        params.put("area", address_string);

        AsyncHttpClient client = new AsyncHttpClient();
        client.post(request_url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i("JSON", "JSON is " + response.toString());
                Log.i("JSON", "Status  code" + statusCode);
                Log.i("JSON", response.toString());

                Log.i("JSON", "Profile data updated successfully");

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                Log.i("JSON", "Status code" + statusCode);
                Log.i("JSON", errorResponse.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "Request Success :)", Toast.LENGTH_SHORT).show();
                Log.i("JSON", "Status  code" + statusCode);
                Log.i("JSON", responseString);

            }
        });

    }

    //Function used to retrieve the user email address
    private void getEmail() {
        String personName = "";
        getFbInfo();
        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
        if (acct != null) {
            personName = acct.getGivenName();
            email = acct.getEmail();
            pname.setText(personName);
        }

        TextView email_view = findViewById(R.id.email);
        email_view.setText(email);

        TextView person_view = findViewById(R.id.usernam);
        person_view.setText(personName);
    }


    private void profile() {
        String profileurl = "https://bloodtransfer.herokuapp.com/index.php/data/profile";


        AsyncHttpClient client = new AsyncHttpClient();

        Log.i("JSON", email);
        RequestParams params = new RequestParams();
        params.put("email", email);

        client.post(profileurl, params, new

                JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Log.i("JSON", "JSON is " + response.toString());
                        Log.i("JSON", "Status  code" + statusCode);
                        Log.i("JSON", response.toString());
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject
                            errorResponse) {
                        Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                        Log.i("JSON", "Status code" + statusCode);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable
                            throwable) {
                        Log.i("JSON", "Status  code" + statusCode);
                        Log.i("JSON", responseString);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        Log.i("JSON", response.toString());
                        Log.i("JSON", "Length" + response.length());
                        fillDetails(response);
                    }
                });
    }

    //Used to fill the mobile number and date of birth in the user profile layout
    private void fillDetails(JSONArray response) {
        if (response.length() > 0) {
            try {
                contact.setText(response.getJSONObject(0).getString("mobile"));
                String date_str = response.getJSONObject(0).getString("dob");
                if (date_str.length()>4)
                    date.setText(date_str);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void getFbInfo() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();

        if (isLoggedIn) {
            GraphRequest request = GraphRequest.newMeRequest(
                    AccessToken.getCurrentAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            try {
                                TextView person_view = findViewById(R.id.usernam);
                                person_view.setText(object.getString("first_name") + " " + object.getString("last_name"));

                                pname.setText(object.getString("first_name") + " " + object.getString("last_name"));
                                if (object.has("email")) {
                                    email = object.getString("email");
                                    TextView email_view = findViewById(R.id.email);
                                    email_view.setText(email);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "error in name", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,first_name,last_name,email,gender,birthday"); // id,first_name,last_name,email,gender,birthday,cover,picture.type(large)
            request.setParameters(parameters);
            request.executeAsync();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), HomeScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

}

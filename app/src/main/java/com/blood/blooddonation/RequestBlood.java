package com.blood.blooddonation;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;

public class RequestBlood extends AppCompatActivity {

    ImageView aposi, anega, bnega, bposi, oposi, onega, abposi, abnega, male, female;
    int i = 1, j = 1;
    boolean sel = false, platelete = false, donavail = true;
    TextView date;
    Button submit;
    TextView location;
    int profile_id = 123;
    String location_string, place_name, placeId, name_string = "Name", google_map_link, bloodgroup;
    DatePickerDialog.OnDateSetListener dateSetListener;
    EditText name, hospital, phonenum, unit;
    ImageButton back;
    double lat,lati;
    double lon,longi;
    LatLng placelatlng;
    String phone_number, email, plate, visibility = "0",area;
    Location location2, location3;
    private LocationManager locationManager;
    int STORAGE_PERMISSION_CODE = 1;
    private String placeAdress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_blood);


        unit = findViewById(R.id.bloodunits);
        aposi = findViewById(R.id.apos);
        anega = findViewById(R.id.aneg);
        bnega = findViewById(R.id.bneg);
        bposi = findViewById(R.id.bpos);
        oposi = findViewById(R.id.opos);
        onega = findViewById(R.id.oneg);
        abposi = findViewById(R.id.abpos);
        abnega = findViewById(R.id.abneg);

        name = findViewById(R.id.nam);
        location = findViewById(R.id.are);
        phonenum = findViewById(R.id.ph);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        } else {
            requestPermissions();
        }
        location2 = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        location2 = getLastKnownLocation();

        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
        if (acct != null) {
            name.setText(acct.getGivenName());
            name_string = acct.getDisplayName();
            email = acct.getEmail();
            profile();
        }
        getFbInfo();

        date = findViewById(R.id.dat);

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int date = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(RequestBlood.this, android.R.style.Theme_Holo_Dialog_NoActionBar_MinWidth, dateSetListener, year, month, date);
                Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;

                String dat = year + "/" + month + "/" + dayOfMonth;
                date.setText(dat);
            }
        };


        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                get_city();
                location_string = location.getText().toString();
            }
        });


        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

//        String[] bloodfor = {"Friend", "Father", "Mother", "Relative", "Others"};
//        final Spinner spin1 = (Spinner) findViewById(R.id.spinner2);
//        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, bloodfor);
//        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spin1.setAdapter(adapter1);

//        String[] citypref = {"Mumbai", "Chennai", "Visakhapatnam", "Hyderabad", "Bangalore"};
//        Spinner spin2 = (Spinner) findViewById(R.id.spinner2);
//        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, citypref);
//        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spin2.setAdapter(adapter2);

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });

        aposi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (j == 1 || sel) {
                    aposi.setImageResource(R.drawable.cgroup310);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    anega.setImageResource(R.drawable.group311);
                    bnega.setImageResource(R.drawable.group312);
                    bposi.setImageResource(R.drawable.group319);
                    oposi.setImageResource(R.drawable.group313);
                    onega.setImageResource(R.drawable.group318);
                    abposi.setImageResource(R.drawable.group317);
                    abnega.setImageResource(R.drawable.group316);

                    bloodgroup = "A+";
                } else {
                    sel = false;
                    aposi.setImageResource(R.drawable.group310);
                    j = 1;
                }
            }
        });

        anega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                anega.setImageResource(R.drawable.cgroup311);
                if (j == 1 || sel) {
                    anega.setImageResource(R.drawable.cgroup311);

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    bnega.setImageResource(R.drawable.group312);
                    bposi.setImageResource(R.drawable.group319);
                    oposi.setImageResource(R.drawable.group313);
                    onega.setImageResource(R.drawable.group318);
                    abposi.setImageResource(R.drawable.group317);
                    abnega.setImageResource(R.drawable.group316);

                    j = 0;
                    bloodgroup = "A-";
                } else {

                    sel = false;
                    anega.setImageResource(R.drawable.group311);
                    j = 1;
                }
            }
        });

        bnega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bnega.setImageResource(R.drawable.cgroup312);
                if (j == 1 || sel) {
                    bnega.setImageResource(R.drawable.cgroup312);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    anega.setImageResource(R.drawable.group311);
                    bposi.setImageResource(R.drawable.group319);
                    oposi.setImageResource(R.drawable.group313);
                    onega.setImageResource(R.drawable.group318);
                    abposi.setImageResource(R.drawable.group317);
                    abnega.setImageResource(R.drawable.group316);

                    bloodgroup = "B-";
                } else {
                    sel = false;
                    bnega.setImageResource(R.drawable.group312);
                    j = 1;
                    bloodgroup = null;
                }
            }
        });

        bposi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bposi.setImageResource(R.drawable.cgroup319);
                if (j == 1 || sel) {
                    bposi.setImageResource(R.drawable.cgroup319);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    bnega.setImageResource(R.drawable.group312);
                    anega.setImageResource(R.drawable.group311);
                    oposi.setImageResource(R.drawable.group313);
                    onega.setImageResource(R.drawable.group318);
                    abposi.setImageResource(R.drawable.group317);
                    abnega.setImageResource(R.drawable.group316);
                    bloodgroup = "B+";
                } else {
                    sel = false;
                    bposi.setImageResource(R.drawable.group319);
                    j = 1;
                    bloodgroup = null;
                }
            }
        });

        oposi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oposi.setImageResource(R.drawable.cgroup313);
                if (j == 1 || sel) {
                    oposi.setImageResource(R.drawable.cgroup313);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    bnega.setImageResource(R.drawable.group312);
                    bposi.setImageResource(R.drawable.group319);
                    anega.setImageResource(R.drawable.group311);
                    onega.setImageResource(R.drawable.group318);
                    abposi.setImageResource(R.drawable.group317);
                    abnega.setImageResource(R.drawable.group316);

                    bloodgroup = "O+";
                } else {

                    sel = false;
                    oposi.setImageResource(R.drawable.group313);
                    j = 1;
                    bloodgroup = null;
                }
            }
        });

        onega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onega.setImageResource(R.drawable.cgroup318);
                if (j == 1 || sel) {
                    onega.setImageResource(R.drawable.cgroup318);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    bnega.setImageResource(R.drawable.group312);
                    bposi.setImageResource(R.drawable.group319);
                    oposi.setImageResource(R.drawable.group313);
                    anega.setImageResource(R.drawable.group311);
                    abposi.setImageResource(R.drawable.group317);
                    abnega.setImageResource(R.drawable.group316);

                    bloodgroup = "O-";
                } else {

                    sel = false;
                    onega.setImageResource(R.drawable.group318);
                    j = 1;
                    bloodgroup = null;
                }
            }
        });

        abposi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abposi.setImageResource(R.drawable.cgroup317);
                if (j == 1 || sel) {
                    abposi.setImageResource(R.drawable.cgroup317);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    bnega.setImageResource(R.drawable.group312);
                    bposi.setImageResource(R.drawable.group319);
                    oposi.setImageResource(R.drawable.group313);
                    onega.setImageResource(R.drawable.group318);
                    anega.setImageResource(R.drawable.group311);
                    abnega.setImageResource(R.drawable.group316);

                    bloodgroup = "AB+";
                } else {

                    sel = false;
                    abposi.setImageResource(R.drawable.group317);
                    j = 1;
                    bloodgroup = null;
                }
            }
        });

        abnega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abnega.setImageResource(R.drawable.cgroup316);
                if (j == 1 || sel) {
                    abnega.setImageResource(R.drawable.cgroup316);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    bnega.setImageResource(R.drawable.group312);
                    bposi.setImageResource(R.drawable.group319);
                    oposi.setImageResource(R.drawable.group313);
                    onega.setImageResource(R.drawable.group318);
                    abposi.setImageResource(R.drawable.group317);
                    anega.setImageResource(R.drawable.group311);

                    bloodgroup = "AB-";
                } else {
                    sel = false;
                    abnega.setImageResource(R.drawable.group316);
                    j = 1;
                }
            }
        });

        //Autocomplete Place Retrieval using Google Place API
        Places.initialize(getApplicationContext(), "AIzaSyC1u7yPHGjEvVUPFpCo6P_aBOrBNDlJKIk");
        // Initialize the AutocompleteSupportFragment.
        //AIzaSyBkaLx2O-8KvvNjrgpMjEa0pCp9yai4eyc
        final AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS,Place.Field.LAT_LNG));

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i("Add", "Place: " + place.getName() + ", " + place.getId());

                place_name = place.getName();
                placeId = place.getId();
                placeAdress = place.getAddress();
                Log.i("JSON", placeAdress);
                Log.i("JSON", "The latlng is "+place.getLatLng());
                placelatlng = place.getLatLng();
                lati = placelatlng.latitude;
                longi = placelatlng.longitude;
                Geocoder geocoder = new Geocoder(getApplicationContext());
                try {
                    List<Address> addresses = geocoder.getFromLocation(lati,longi,1);
                    if(addresses!=null && addresses.size()>0){
                        Address address2 = addresses.get(0);
                        area = address2.getSubLocality();
                        Log.i("JSON",address2.getAdminArea()+"  "+address2.getSubLocality()+"   "+address2.getSubAdminArea()+"   "+address2.getLocale());
                    }else{
                        Log.i("JSON","OOPS");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("Add", "An error occurred: " + status);
            }
        });

        RadioGroup rg = (RadioGroup) findViewById(R.id.platelet1);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.yes:
                        // do operations specific to this selection
                        plate = "yes";
                        break;
                    case R.id.no:
                        // do operations specific to this selection
                        plate = "no";
                        break;

                }
            }
        });


        submit = findViewById(R.id.sub);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (haveNetworkConnection()) {
                    submit.setText("Submitting..");
                    submit.setClickable(false);

                    phone_number = phonenum.getText().toString();
                    name_string = name.getText().toString();

//                request("friend", location_string, bloodgroup, name_string, place_name + ", " + placeAdress, place_name + ", " + placeId, phone_number);
                    request("friend", location_string, bloodgroup, name_string, "Hyderabad" + ", " + "Hyderabad", "Hospital", phone_number);
                } else {
                    noNetworkAction();
                }

            }
        });

    }

    //Used to request location permission
    private void requestPermissions() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            new AlertDialog.Builder(this)
                    .setTitle("Permission Needed")
                    .setMessage("This permission is needed to fetch your current city")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(RequestBlood.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, STORAGE_PERMISSION_CODE);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create().show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, STORAGE_PERMISSION_CODE);
        }
    }

    //Used for retrieving the current user city
    private void get_city() {
        if (location2 != null) {
            lat = location2.getLatitude();
            lon = location2.getLongitude();
            try {
                Log.i("JSON", "Lat Lon" + lat + " " + lon);
                Geocoder geocoder = new Geocoder(this);
                List<Address> addresses = null;
                addresses = geocoder.getFromLocation(lat, lon, 1);
                String cit = addresses.get(0).getLocality();
                location.setText(cit);
                Log.i("Location", "Locality: " + addresses.get(0).getLocality() + "\n Pincode" + addresses.get(0).getPostalCode() + "\n Premises" + addresses.get(0).getPremises() + "\n Admin area " + addresses.get(0).getAdminArea() + "\n Subadmin area" + addresses.get(0).getSubAdminArea() + "\n Locale" + addresses.get(0).getLocale() + "\n SubLocalility " + addresses.get(0).getSubLocality()
                );
                google_map_link = lat + "," + lon;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Log.i("JSON", "LOcation problem");
            Toast.makeText(this, "Location Not found, please enter hospital address.", Toast.LENGTH_SHORT).show();
        }

    }

    //Function which sends all the data to the database which is used for filtering the donors
    private void request(String toString, String city, final String bloodgroup, final String name, final String location, final String hospital, final String phonenum) {

        final RequestParams params = new RequestParams();

        Log.i("plat", String.valueOf(platelete));

        String plat_val;
        String uni = unit.getText().toString();
        if (platelete)
            plat_val = "yes";
        else
            plat_val = "no";

        Log.i("JSON", String.valueOf(profile_id) + "\nname: " + name + "\nwhatsapp: null\nmobile: " + phonenum + "\nrelation: " + toString + "\nneededby: skdafjhk\nbloodgrp: " + bloodgroup + "\ncity: Hyderabad\narea: " + location + "\ngoogle_loc: " + google_map_link + "\n Status: OPEN\nhospital :" + hospital + "\ndate: " + date.getText() + "\nplatelets: " + plat_val + "\nreq_type: NORMAL");

        params.put("id", profile_id);
        params.put("name", name);
        params.put("whatsapp", "null");
        params.put("mobile", phonenum);
        params.put("units", uni);
        params.put("needed_by", "lkjdsl");
        params.put("bloodgrp", bloodgroup);
        params.put("city", location_string);
        params.put("area", area);
        params.put("google_loc", google_map_link);
        params.put("status", "OPEN");
        params.put("hospital", hospital);
        params.put("date", date.getText());
        params.put("platelets", plate);
        params.put("req_type", "NORMAL");

        final String fcity = city;
        final String fbloodgroup = bloodgroup;

        final String request_url = "https://bloodtransfer.herokuapp.com/index.php/data/reciever";
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(request_url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i("JSON", "JSON is " + response.toString());
                Log.i("JSON", "Status  code" + statusCode);
                Log.i("filtered", response.toString());
                JSONArray donors = null;
                int request_id = 0;
                try {
                    Log.i("JSON", response.getString("donors"));
                    if (response.getString("donors").equals("No donors found with your preferences")) {
                        Log.i("JSON", "No donors");
                        donavail = true;
                    } else {
                        donavail = false;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if (donavail) {
                    Toast.makeText(getApplicationContext(), "Donors are not available", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), HomeScreen.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                } else {

                    try {
                        request_id = (int) response.get("request_id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        donors = response.getJSONArray("donors");
                        Log.i("JSON", "The donors" + donors.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.i("req", "New variables" + request_id);

                    Toast.makeText(getApplicationContext(), "Request Success :)", Toast.LENGTH_SHORT).show();

                    Log.i("JSON", "The filtered donors are " + response.toString());

                    Log.i("JSON", "The profile id is " + String.valueOf(profile_id));

                    Intent intent = new Intent(getApplicationContext(), FilteredResult.class);
                    assert donors != null;
                    intent.putExtra("json", donors.toString());
                    intent.putExtra("rname", name);
                    intent.putExtra("request_id", String.valueOf(request_id));
                    intent.putExtra("blood", fbloodgroup);
                    intent.putExtra("hospital", location);
                    intent.putExtra("location", location_string);
                    intent.putExtra("google_loc", google_map_link);
                    intent.putExtra("mobile", phonenum);
                    Log.i("Insiide", String.valueOf(profile_id));
                    intent.putExtra("profile_id", String.valueOf(profile_id));

                    startActivity(intent);

                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                Log.i("JSON", "Status code" + statusCode);
                Log.i("JSON", errorResponse.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "Request Success :)", Toast.LENGTH_SHORT).show();
                Log.i("JSON", "Status  code" + statusCode);
                Log.i("filtered", responseString);

                Log.i("JSON", "The filtered donors are " + responseString);

                Toast.makeText(getApplicationContext(), "Request Success :)", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplicationContext(), FilteredResult.class);
                intent.putExtra("json", responseString);
                intent.putExtra("rname", name);
                intent.putExtra("blood", fbloodgroup);
                intent.putExtra("hospital", hospital);
                intent.putExtra("location", location_string);
                intent.putExtra("google_loc", google_map_link);
                intent.putExtra("mobile", phonenum);
                intent.putExtra("profile_id", String.valueOf(profile_id));

                startActivity(intent);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                Log.i("JSON", response.toString());

                Intent intent = new Intent(getApplicationContext(), FilteredResult.class);
                intent.putExtra("json", response.toString());
                intent.putExtra("rname", name);
                intent.putExtra("blood", fbloodgroup);
                intent.putExtra("hospital", location);
                intent.putExtra("location", location_string);
                intent.putExtra("google_loc", google_map_link);
                intent.putExtra("mobile", phonenum);
                intent.putExtra("profile_id", String.valueOf(profile_id));

                startActivity(intent);

            }
        });

    }


    private void profile() {
        String profileurl = "https://bloodtransfer.herokuapp.com/index.php/data/profile";


        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("email", email);

        client.post(profileurl, params, new

                JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Log.i("profile", "JSON is " + response.toString());
                        Log.i("profile", "Status  code" + statusCode);
                        Log.i("profile", response.toString());
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject
                            errorResponse) {
                        Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                        Log.i("profile", "Status code" + statusCode);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable
                            throwable) {
                        Log.i("profile", "Status  code" + statusCode);
                        Log.i("profile", responseString);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        Log.i("profile", response.toString());
                        Log.i("profile", "Length" + response.length());
                        fillDetails(response);
                    }
                });
    }

    private void fillDetails(JSONArray response) {
        if (response.length() > 0) {
            try {
                EditText contact = findViewById(R.id.ph);
                contact.setText(response.getJSONObject(0).getString("mobile"));

                visibility = response.getJSONObject(0).getString("visiblity");

                profile_id = Integer.valueOf(response.getJSONObject(0).getString("ID"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void getFbInfo() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
        if (isLoggedIn) {
            GraphRequest request = GraphRequest.newMeRequest(
                    AccessToken.getCurrentAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            try {
//                            Log.d(LOG_TAG, "fb json object: " + object);
//                            Log.d(LOG_TAG, "fb graph response: " + response);

                                String first_name = object.getString("first_name");
                                String last_name = object.getString("last_name");
//                            String gender = object.getString("gender");
//                            String birthday = object.getString("birthday");
//                            String image_url = "http://graph.facebook.com/" + id + "/picture?type=large";

                                if (first_name != null && last_name != null) {
                                    name.setText(first_name + " " + last_name);
                                    name_string = first_name + " " + last_name;
                                }
                                if (object.has("email")) {
                                    email = object.getString("email");
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "error in name", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,first_name,last_name,email,gender,birthday"); // id,first_name,last_name,email,gender,birthday,cover,picture.type(large)
            request.setParameters(parameters);
            request.executeAsync();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), HomeScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    //Used to check whether the device is connected to the internet or not
    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    private void noNetworkAction() {
        final Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                "No internet connection.",
                Snackbar.LENGTH_INDEFINITE);
        snackbar.setActionTextColor(ContextCompat.getColor(Objects.requireNonNull(getApplicationContext()),
                R.color.black));
        snackbar.setAction(R.string.try_again, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //recheck internet connection and call DownloadJson if there is internet
                if (!haveNetworkConnection())
                    noNetworkAction();
                else {
                    Intent intent = getIntent();
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    finish();
                    startActivity(intent);
                }
            }
        }).show();
    }

    //Used for accessing the current location using the location provider
    private Location getLastKnownLocation() {
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
            } else {
                requestPermissions();
            }
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }
}

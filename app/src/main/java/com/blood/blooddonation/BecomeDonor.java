package com.blood.blooddonation;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;

public class BecomeDonor extends AppCompatActivity {

    ImageView aposi, anega, bnega, bposi, oposi, onega, abposi, abnega, male, female;
    int i = 1, j = 1;
    boolean ma = false, fe = false, sel = false, doborldd = true;
    EditText name, phone;
    TextView age;

    String gender, bloodgroup;
    CheckBox visibility;
    DatePickerDialog.OnDateSetListener dateSetListener;
    String dat, personalemail;
    double lat;
    double lon;
    Location location;
    private LocationManager locationManager;
    int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    Button sub_button;
    private String google_map_link;
    private String place_string;
    private String cityName;
    private String address_string;
    private TextView date;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);


        setContentView(R.layout.activity_become_donor);


        //ImageButtons of blood groups.
        aposi = findViewById(R.id.apos);
        anega = findViewById(R.id.aneg);
        bnega = findViewById(R.id.bneg);
        bposi = findViewById(R.id.bpos);
        oposi = findViewById(R.id.opos);
        onega = findViewById(R.id.oneg);
        abposi = findViewById(R.id.abpos);
        abnega = findViewById(R.id.abneg);

        // ImageButtons of gender.
        male = findViewById(R.id.imageView);
        female = findViewById(R.id.imageView2);

        //Location manager and code to request location permission if necessary.
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(BecomeDonor.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            return;
        }

        // Back button code for back button.
        ImageButton imageButton = findViewById(R.id.back);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });


        // Gettting last known location from network and saved in location variable ( Location Object ).
        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        

        // Name,age,phone definition

        name = findViewById(R.id.editText);
        //city = findViewById(R.id.city);
        age = findViewById(R.id.age_donor);
        phone = findViewById(R.id.phonenumber);

        //Check box
        visibility = findViewById(R.id.checkBox2);


        date = findViewById(R.id.date);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int date = cal.get(Calendar.DAY_OF_MONTH);
                doborldd = true;
                DatePickerDialog dialog = new DatePickerDialog(BecomeDonor.this, android.R.style.Theme_Holo_Dialog_NoActionBar_MinWidth, dateSetListener, year, month, date);
                Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        //Listener for age and date of blood donation.

        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;

                dat = year + "/" + month + "/" + dayOfMonth;

                if (doborldd)
                    date.setText(dat);
                else
                    age.setText(dat);

            }
        };


        age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int date = cal.get(Calendar.DAY_OF_MONTH);

                doborldd = false;
                DatePickerDialog dialog = new DatePickerDialog(BecomeDonor.this, android.R.style.Theme_Holo_Dialog_NoActionBar_MinWidth, dateSetListener, year, month, date);
                Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });



        male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (i == 1) {
                    male.setImageResource(R.drawable.cman);
                    i = 0;
                    ma = true;
                    gender = "male";
                } else if (i == 0) {
                    male.setImageResource(R.drawable.cman);
                    ma = true;
                    female.setImageResource(R.drawable.woman);
                }

            }
        });


        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (i == 1) {
                    female.setImageResource(R.drawable.cwomen);
                    fe = true;
                    gender = "female";
                    i = 0;
                } else if (i == 0) {
                    female.setImageResource(R.drawable.cwomen);
                    fe = true;
                    male.setImageResource(R.drawable.man1);

                }
            }
        });

        // COde for getting the selected blood group.

        aposi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (j == 1 || sel) {
                    // Selecting the clicked blood group to active state
                    aposi.setImageResource(R.drawable.cgroup310);
                    j = 0;

                    sel = true;
                    //Set all other blood groups to blank
                    anega.setImageResource(R.drawable.group311);
                    bnega.setImageResource(R.drawable.group312);
                    bposi.setImageResource(R.drawable.group319);
                    oposi.setImageResource(R.drawable.group313);
                    onega.setImageResource(R.drawable.group318);
                    abposi.setImageResource(R.drawable.group317);
                    abnega.setImageResource(R.drawable.group316);

                    bloodgroup = "A+";
                } else {
                    sel = false;
                    aposi.setImageResource(R.drawable.group310);
                    j = 1;
                }
            }
        });

        anega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                anega.setImageResource(R.drawable.cgroup311);
                if (j == 1 || sel) {

                    // Selecting the clicked blood group to active state
                    anega.setImageResource(R.drawable.cgroup311);

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    bnega.setImageResource(R.drawable.group312);
                    bposi.setImageResource(R.drawable.group319);
                    oposi.setImageResource(R.drawable.group313);
                    onega.setImageResource(R.drawable.group318);
                    abposi.setImageResource(R.drawable.group317);
                    abnega.setImageResource(R.drawable.group316);

                    j = 0;
                    bloodgroup = "A-";
                } else {

                    sel = false;
                    anega.setImageResource(R.drawable.group311);
                    j = 1;
                }
            }
        });

        bnega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bnega.setImageResource(R.drawable.cgroup312);
                if (j == 1 || sel) {
                    bnega.setImageResource(R.drawable.cgroup312);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    anega.setImageResource(R.drawable.group311);
                    bposi.setImageResource(R.drawable.group319);
                    oposi.setImageResource(R.drawable.group313);
                    onega.setImageResource(R.drawable.group318);
                    abposi.setImageResource(R.drawable.group317);
                    abnega.setImageResource(R.drawable.group316);

                    bloodgroup = "B-";
                } else {
                    sel = false;
                    bnega.setImageResource(R.drawable.group312);
                    j = 1;
                    bloodgroup = null;
                }
            }
        });

        bposi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bposi.setImageResource(R.drawable.cgroup319);
                if (j == 1 || sel) {
                    bposi.setImageResource(R.drawable.cgroup319);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    bnega.setImageResource(R.drawable.group312);
                    anega.setImageResource(R.drawable.group311);
                    oposi.setImageResource(R.drawable.group313);
                    onega.setImageResource(R.drawable.group318);
                    abposi.setImageResource(R.drawable.group317);
                    abnega.setImageResource(R.drawable.group316);
                    bloodgroup = "B+";
                } else {
                    sel = false;
                    bposi.setImageResource(R.drawable.group319);
                    j = 1;
                    bloodgroup = null;
                }
            }
        });

        oposi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oposi.setImageResource(R.drawable.cgroup313);
                if (j == 1 || sel) {
                    oposi.setImageResource(R.drawable.cgroup313);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    bnega.setImageResource(R.drawable.group312);
                    bposi.setImageResource(R.drawable.group319);
                    anega.setImageResource(R.drawable.group311);
                    onega.setImageResource(R.drawable.group318);
                    abposi.setImageResource(R.drawable.group317);
                    abnega.setImageResource(R.drawable.group316);

                    bloodgroup = "O+";
                } else {

                    sel = false;
                    oposi.setImageResource(R.drawable.group313);
                    j = 1;
                    bloodgroup = null;
                }
            }
        });

        onega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onega.setImageResource(R.drawable.cgroup318);
                if (j == 1 || sel) {
                    onega.setImageResource(R.drawable.cgroup318);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    bnega.setImageResource(R.drawable.group312);
                    bposi.setImageResource(R.drawable.group319);
                    oposi.setImageResource(R.drawable.group313);
                    anega.setImageResource(R.drawable.group311);
                    abposi.setImageResource(R.drawable.group317);
                    abnega.setImageResource(R.drawable.group316);

                    bloodgroup = "O-";
                } else {

                    sel = false;
                    onega.setImageResource(R.drawable.group318);
                    j = 1;
                    bloodgroup = null;
                }
            }
        });

        abposi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abposi.setImageResource(R.drawable.cgroup317);
                if (j == 1 || sel) {
                    abposi.setImageResource(R.drawable.cgroup317);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    bnega.setImageResource(R.drawable.group312);
                    bposi.setImageResource(R.drawable.group319);
                    oposi.setImageResource(R.drawable.group313);
                    onega.setImageResource(R.drawable.group318);
                    anega.setImageResource(R.drawable.group311);
                    abnega.setImageResource(R.drawable.group316);

                    bloodgroup = "AB+";
                } else {

                    sel = false;
                    abposi.setImageResource(R.drawable.group317);
                    j = 1;
                    bloodgroup = null;
                }
            }
        });

        abnega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abnega.setImageResource(R.drawable.cgroup316);
                if (j == 1 || sel) {
                    abnega.setImageResource(R.drawable.cgroup316);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    bnega.setImageResource(R.drawable.group312);
                    bposi.setImageResource(R.drawable.group319);
                    oposi.setImageResource(R.drawable.group313);
                    onega.setImageResource(R.drawable.group318);
                    abposi.setImageResource(R.drawable.group317);
                    anega.setImageResource(R.drawable.group311);

                    bloodgroup = "AB-";
                } else {
                    sel = false;
                    abnega.setImageResource(R.drawable.group316);
                    j = 1;
                }
            }
        });

        sub_button = findViewById(R.id.button);

        sub_button.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {

                // Code to check whether he/she is above 18 yrs or not. If not this code is used to display the no of days remaining to
                // become 18 yrs old.

                Date c = Calendar.getInstance().getTime();
                System.out.println("Current time => " + c);

                @SuppressLint("SimpleDateFormat") SimpleDateFormat myFormat = new SimpleDateFormat("yyyy/MM/dd");

                String formattedDate = myFormat.format(c);
                String dateBeforeString = age.getText().toString();

                Log.i("JSON",formattedDate+" "+dateBeforeString);
                float daysBetween = 6590;
                try {
                    Date dateBefore = myFormat.parse(dateBeforeString);
                    Date dateAfter = myFormat.parse(formattedDate);
                    long difference = dateAfter.getTime() - dateBefore.getTime();

                    daysBetween = (difference / (1000*60*60*24));
                    Log.i("JSON", String.valueOf(daysBetween));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (daysBetween < 6570){
                    Toast.makeText(getApplicationContext(),"You can donate after "+(6570-daysBetween)+" days",Toast.LENGTH_LONG).show();
                }
                else {
                    sub_button.setText("Registering..");
                    sub_button.setClickable(false);
                    addPost(name.getText().toString(), cityName, gender, bloodgroup, visibility.isChecked());
                }
            }
        });


        //Autocomplete Place Retrieval using Google Place API
        Places.initialize(getApplicationContext(), "AIzaSyC1u7yPHGjEvVUPFpCo6P_aBOrBNDlJKIk");
        // Initialize the AutocompleteSupportFragment.
        final AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        // Specify the types of place data to return.
        assert autocompleteFragment != null;
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.LAT_LNG,Place.Field.NAME,Place.Field.ADDRESS));

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i("Add", "Place: " + place.getName() + ", " + place.getId());
                place_string = place.getName();

                LatLng latlng = place.getLatLng();
                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                try {
                    List<Address> addresses = geocoder.getFromLocation(latlng.latitude, latlng.longitude, 1);
                    cityName = addresses.get(0).getLocality();
                    google_map_link = latlng.latitude + "," + latlng.longitude;
                    address_string= addresses.get(0).getSubLocality();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.i("JSON",cityName+" "+google_map_link);
            }

            @Override
            public void onError(@NonNull Status status) {
                // TODO: Handle the error.
                Log.i("Add", "An error occurred: " + status);
            }
        });


        //Code used to check whether the user signed in with google or not. If signed in get the name and email from Google API
        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
        if (acct != null) {
            name.setText(acct.getDisplayName());
            personalemail = acct.getEmail();
            profile();
        } else {
            // If not signed in with google check Facebook login and fetch details from it.
            getFbInfo();
        }
    }


    // Method for getting the city of current location.
    private void get_city() {
        lat = location.getLatitude();
        lon = location.getLongitude();
        try {
            Geocoder geocoder = new Geocoder(this);
            List<Address> addresses = null;
            addresses = geocoder.getFromLocation(lat, lon, 1);

            // addresses.get(0).getLocality() returns city as string.

            String cit = addresses.get(0).getLocality();
            Log.i("Location", "Locality: " + addresses.get(0).getLocality() + "\n Pincode" + addresses.get(0).getPostalCode() + "\n Premises" + addresses.get(0).getPremises() + "\n Admin area " + addresses.get(0).getAdminArea() + "\n Subadmin area" + addresses.get(0).getSubAdminArea() + "\n Locale" + addresses.get(0).getLocale() + "\n SubLocalility " + addresses.get(0).getSubLocality()
            );
            String add = addresses.get(0).getAddressLine(0);
            Log.i("Add", add);
            cityName = cit;
            google_map_link = "https://www.google.com/maps?q=" + lat + "," + lon;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    // addPost is used to update the user as donor in user table. Takes all parameters from becomedonor and sends it to becomedonor() in database.
    public void addPost(final String name, final String email, final String gender, final String bloodgroup, final boolean visibility_str) {

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        final RequestParams params = new RequestParams();

        if (account != null) {
            //Executes if user is signed in with google.

            int vis;
            if(visibility_str)
                params.put("visibility", 1);
            else
                params.put("visibility",0);
            personalemail = account.getEmail();

            Log.i("JSON", name + " " + personalemail + " " + gender + " " + bloodgroup + " " + visibility_str + " " + age.getText().toString() + " " + phone.getText().toString() + " " + dat+" "+cityName+" "+google_map_link+" "+address_string);
            params.put("name", name);
            params.put("mobile", phone.getText().toString());
            params.put("donor", "yes");
            params.put("gender", gender);
            params.put("email", personalemail);
            params.put("dob", age.getText().toString());
            params.put("city", cityName);
            params.put("area",address_string);
            params.put("bloodgrp", bloodgroup);
            params.put("lastdonated", date.getText().toString());
            params.put("google_loc",google_map_link);

            String donor_url = "https://bloodtransfer.herokuapp.com/index.php/data/becomedonor";

            AsyncHttpClient client = new AsyncHttpClient();
            client.post(donor_url, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.i("JSON", "JSON is " + response.toString());
                    Log.i("JSON", "Status  code" + statusCode);
                    Log.i("JSON", response.toString());
                    int unicode = 0x1F60D;
                    Toast.makeText(getApplicationContext(), "You are a hero now " + new String(Character.toChars(unicode)), Toast.LENGTH_LONG).show();
                    onBackPressed();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                    Log.i("JSON", "Status code" + statusCode);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    int unicode = 0x1F60D;
                    Toast.makeText(getApplicationContext(), "You are a hero now " + new String(Character.toChars(unicode)), Toast.LENGTH_LONG).show();
                    Log.i("JSON", "Status  code" + statusCode);
                    Log.i("JSON", responseString);
                    onBackPressed();
                }
            });

        } else {
            // Executes if user is signed in with Facebook.
            GraphRequest request = GraphRequest.newMeRequest(
                    AccessToken.getCurrentAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            try {
                                // Gets details from facebook from fills the fields in the UI.
                                if (object.has("email")) {
                                    personalemail = object.getString("email");
                                    Log.i("JSON", name + " " + personalemail + " " + gender + " " + bloodgroup + " " + visibility_str + " " + age.getText().toString() + " " + phone.getText().toString() + " " + dat);
                                    params.put("name", name);
                                    params.put("mobile", phone.getText().toString());
                                    params.put("donor", "yes");
                                    params.put("visibility", visibility_str);
                                    params.put("gender", gender);
                                    params.put("email", personalemail);
                                    params.put("dob", age.getText().toString());
                                    params.put("city", cityName);
                                    params.put("bloodgrp", bloodgroup);
                                    params.put("lastdonated", dat);
                                    params.put("google_loc",google_map_link);


                                    Log.i("JSON", "Has email" + personalemail);
                                    String donor_url = "https://bloodtransfer.herokuapp.com/index.php/data/becomedonor";

                                    AsyncHttpClient client = new AsyncHttpClient();
                                    client.post(donor_url, params, new JsonHttpResponseHandler() {
                                        @Override
                                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                            Log.i("JSON", "JSON is " + response.toString());
                                            Log.i("JSON", "Status  code" + statusCode);
                                            Log.i("JSON", response.toString());
                                            int unicode = 0x1F60D;
                                            Toast.makeText(getApplicationContext(), "You are a hero now " + new String(Character.toChars(unicode)), Toast.LENGTH_LONG).show();
                                            onBackPressed();
                                        }

                                        @Override
                                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                                            Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                                            Log.i("JSON", "Status code" + statusCode);
                                        }

                                        @Override
                                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                            int unicode = 0x1F60D;
                                            Log.i("JSON", "Status  code" + statusCode);
                                            Log.i("JSON", responseString);
                                            Toast.makeText(getApplicationContext(), "You are a hero now " + new String(Character.toChars(unicode)), Toast.LENGTH_LONG).show();
                                            onBackPressed();

                                        }
                                    });
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "error in name", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,first_name,last_name,email,gender,birthday"); // id,first_name,last_name,email,gender,birthday,cover,picture.type(large)
            request.setParameters(parameters);
            request.executeAsync();
        }
        Log.i("JSON", "reached addPost method");


    }

    private void profile() {
        //Profile API is used to save/get the information of the user. In this activity profile is used to get user's phone number and gender status

        // profile() is called in the database with email as parameter.

        String profileurl = "https://bloodtransfer.herokuapp.com/index.php/data/profile";


        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("email", personalemail);

        client.post(profileurl, params, new

                JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Log.i("JSON", "JSON is " + response.toString());
                        Log.i("JSON", "Status  code" + statusCode);
                        Log.i("JSON", response.toString());
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject
                            errorResponse) {
                        Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                        Log.i("JSON", "Status code" + statusCode);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable
                            throwable) {
                        Log.i("JSON", "Status  code" + statusCode);
                        Log.i("JSON", responseString);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        Log.i("JSON", response.toString());
                        Log.i("JSON", "Length" + response.length());
                        fillDetails(response);
                    }
                });
    }

    private void fillDetails(JSONArray response) {

        // Fills the details such as age and phone number in  the UI.

        if (response.length() > 0) {
            try {
                EditText contact = findViewById(R.id.phonenumber);
                contact.setText(response.getJSONObject(0).getString("mobile"));

                if(response.getJSONObject(0).has("gender")){
                    String temp = response.getJSONObject(0).getString("gender");
                    if(temp.equals("male"))
                        male.setImageResource(R.drawable.cman);
                    else if(temp.equals("female"))
                        female.setImageResource(R.drawable.cwomen);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), HomeScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    private void getFbInfo() {

        // Used for fetching first name, last name and email from the facebook.

        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        try {
//                            Log.d(LOG_TAG, "fb json object: " + object);
//                            Log.d(LOG_TAG, "fb graph response: " + response);

                            String first_name = object.getString("first_name");
                            String last_name = object.getString("last_name");
//                            String gender = object.getString("gender");
//                            String birthday = object.getString("birthday");
//                            String image_url = "http://graph.facebook.com/" + id + "/picture?type=large";

                            if (first_name != null && last_name != null) {
                                name.setText(first_name + " " + last_name);

                            }
                            if (object.has("email")) {
                                personalemail = object.getString("email");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "error in name", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,last_name,email,gender,birthday"); // id,first_name,last_name,email,gender,birthday,cover,picture.type(large)
        request.setParameters(parameters);
        request.executeAsync();
    }

}
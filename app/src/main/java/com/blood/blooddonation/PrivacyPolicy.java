package com.blood.blooddonation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class PrivacyPolicy extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);

        ImageButton back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });

        //Textview used to display the privacy policy to the user
        TextView text = findViewById(R.id.text);

        text.setText(Html.fromHtml("<p>Blood for Need is an App/Website which functions with the sole aim to promote and spread the awareness among the people to donate blood which is helpful for the poor and needy, support child education.</p>" +
                "<h3>Contacting Blood Donors</h3><p>The users need to utilise the information provided in the App/Website and approach the list of donors.</p><p>The details of the donors are displayed only to facilitate the users for contacting them with the interest of Donor only when in need of blood in case of any emergency.</p>" +
                "<p>Blood for Need App/Website is not responsible for willingness/unwillingness to donate blood expressed by the donor listed in the App.</p><p>The donor information available on the App/Website is made available to us by the donors and its purely the donor's right to continue or to withdraw any information available at any point of time.</p>" +
                "<h2>Content</h2><p>Content provided in or through the App/Website:</p><p>Is provided for information purposes only.</p><p>Is not shared with third parties.</p><p>The donor information available in this App/Website is made available to us by the donors and its purely the donor's right to continue or to withdraw any information available at any point of time.</p>" +
                "<p>We are not responsible for any inaccuracy in the information available on the App/Website.</p><p>We are not responsible for any misuse of contact information displayed in the App/Website. If you feel that your contact details are being misused then contact us on support@bloodforneed.org</p>" +
                "<h3>Cookies</h3><p>Blood for Need may employ cookie technology to allow subscribers and users to move more quickly through our App/Website. Cookies are small text files a App/Website can use to recognise repeat users and facilitate the user's ongoing access to and use of the App/Website. Generally, cookies do not pose a threat to a user's files." +
                " Blood for Need cannot control the use of cookies by advertisers or third parties hosting data for Blood for Need. In case a member/user does not want the use of cookies, most browsers allow the user to deny or accept the cookie feature.</p>" +
                "<h3>External Links</h3><p>Blood for Need may contain links to other App/Websites or resources only for the convenience of the users. Blood for Need is not responsible for the content of these external App/Websites, nor does Blood for Need endorse, warrant or guarantee the products, services or information described or offered in those App/Websites." +
                " It is the responsibility of the user to examine the copyright and licensing restrictions of linked pages and to secure all neccessary permission.</p>" +
                "<h3>Blood for Need Rights</h3><p>Blood for Need reserves the right to change, modify or discontinue any aspect of the App/Website at any time, including any information or its contents or features. Blood for Need reserves the right to collect, analyze and disseminate the patterns of usage of the App/Website by all its Users.<p>" +
                "<h3>Copyrights & Trademarks</h3><p>All information or content on the App/Website is the exclusive property of Blood for Need. No information or content on the App/Website may be copied, modified, reproduced, republished, uploaded, transmitted, posted or distributed in any form without the prior written consent of Blood for Need Management.</p>" +
                "<p>Blood for Need trademark shall not be used in any manner without the prior written consent of Blood for Need Management.</p><p>Unauthorized use of any information or content App/Websiteearing on the App/Website shall violate copyright, trademark and other applicable laws and could result in criminal or civil penalties.</p>" +
                "<p>We are not responsible for any misuse of contact information given to users via IVRS, SMS and Mobile App/Websites. If you feel that your contact details are being misused then contact us on support@bloodforneed.org</p>"));
    }
}

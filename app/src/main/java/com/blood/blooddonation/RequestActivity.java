package com.blood.blooddonation;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteFragment;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;

public class RequestActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    JSONArray result;
    ListView listView;
    String name, number, city = "City";
    double lat;
    double lon;
    Location location;
    private LocationManager locationManager;
    AutocompleteSupportFragment autocompleteFragment;
    ImageView citychan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);


        Places.initialize(getApplicationContext(), "AIzaSyC1u7yPHGjEvVUPFpCo6P_aBOrBNDlJKIk");
        // Initialize the AutocompleteSupportFragment.

        autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteFragment.getView().setVisibility(View.INVISIBLE);
        // Specify the types of place data to return.
        assert autocompleteFragment != null;
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.LAT_LNG, Place.Field.NAME, Place.Field.ADDRESS));
        citychan = findViewById(R.id.changecity);
        citychan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autocompleteFragment.getView().setVisibility(View.VISIBLE);
                // Set up a PlaceSelectionListener to handle the response.
                autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                    @Override
                    public void onPlaceSelected(Place place) {

                        // TODO: Get info about the selected place.
                        autocompleteFragment.getView().setVisibility(View.INVISIBLE);
                        Geocoder geocoder = new Geocoder(getApplicationContext());
                        List<Address> addresses = null;
                        try {
                            addresses = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        city = addresses.get(0).getLocality();

                        Log.i("JSON","City "+city);
                        displayAllRecievers();

                    }

                    @Override
                    public void onError(Status status) {
                        // TODO: Handle the error.
                        Log.i("Add", "An error occurred: " + status);
                    }
                });

            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar_req);
//        toolbar.setTitle("Red Blood");
        toolbar.setBackgroundColor(getResources().getColor(R.color.main));

        setSupportActionBar(toolbar);
        Window window = getWindow();
        window.setStatusBarColor(Color.parseColor("#FF215D"));

        ImageButton imageButton = findViewById(R.id.arrow);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });
        Button req = findViewById(R.id.button_req);
        req.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RequestBlood.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });

        listView = findViewById(R.id.listview_request);

        if (haveNetworkConnection()) {
            count();
            displayAllRecievers();

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                return;
            }
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            lat = location.getLatitude();
            lon = location.getLongitude();

            get_city();
        } else {
            LottieAnimationView nointernet = findViewById(R.id.no_internet);
            nointernet.setVisibility(View.VISIBLE);
        }
    }

    //Display all the recievers from the database
    private void displayAllRecievers() {
        String displayurl = "https://bloodtransfer.herokuapp.com/index.php/data/allrequests";

        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("city", city);

        client.post(displayurl, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i("JSON", "JSON is " + response.toString());
                Log.i("JSON", "Status  code" + statusCode);
                Log.i("JSON", response.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                Log.i("JSON", "Status code" + statusCode);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i("JSON", "Status  code" + statusCode);
                Log.i("JSON", responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                Log.i("JSON", response.toString());
                Log.i("JSON", "Length " + response.length());
                if (response.length() > 0) {
                    result = response;

                    CustomAdapter2 adapter = new CustomAdapter2();
                    listView.setAdapter(adapter);

                } else {
                    LottieAnimationView animationView = findViewById(R.id.animation_view);
                    animationView.setVisibility(View.VISIBLE);
                    TextView text = findViewById(R.id.no_rec);
                    text.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    //Used to retrieve the requestor count
    private void count() {
        String rec_url = "https://bloodtransfer.herokuapp.com/index.php/data/reqcount";

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        client.post(rec_url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i("JSON", "JSON is " + response.toString());
                Log.i("JSON", response.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.i("JSON", "Status code" + statusCode);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "Request Success:)", Toast.LENGTH_SHORT).show();
                Log.i("JSON", responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                Log.i("JSON", response.toString());
                TextView don_count2;
                don_count2 = findViewById(R.id.don_count);

                try {
                    JSONObject obj = (JSONObject) response.get(0);
                    don_count2.setText(obj.getString("count(*)"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //
    class CustomAdapter2 extends BaseAdapter {
        @Override
        public int getCount() {
            return result.length();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.cardview_request, null);

            TextView name = convertView.findViewById(R.id.name_card_request);
            TextView place = convertView.findViewById(R.id.place_1);
            TextView blood = convertView.findViewById(R.id.blood_group_card_1);
            Button no = convertView.findViewById(R.id.help_1);
            TextView date = convertView.findViewById(R.id.date);
            TextView photo = convertView.findViewById(R.id.photo1);
            Button dir = convertView.findViewById(R.id.help_2);
            TextView req_type = convertView.findViewById(R.id.request_type);
            TextView units = convertView.findViewById(R.id.units);


            String google_loc[] = null;

            try {

                final JSONObject object = result.getJSONObject(position);
                name.setText(object.getString("name"));
                place.setText(object.getString("area")+", "+object.getString("city"));
                blood.setText(object.getString("blood_group"));


                String unit_str = object.getString("units");


                if (unit_str != null) {
                    units.setText("Units: " + unit_str);
                }


                blood.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Log.i("plate", "In filtered " + object.getString("platelets"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                date.setText(object.getString("date"));
                String a = (object.getString("platelets"));
                google_loc = object.getString("google_loc").split(",");
                if (a.equals("yes")) {
                    req_type.setText("Platelets");
                } else {
                    req_type.setText("Blood");
                }

                final String[] finalGoogle_loc = google_loc;
                dir.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                                Uri.parse("http://maps.google.com/maps?saddr=" + lat + "," + lon + "&daddr=" + finalGoogle_loc[0] + "," + finalGoogle_loc[1]));
                        startActivity(intent);
                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            intent.setData(Uri.parse("tel:" + object.getString("contact_no")));
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });


                String fl = object.getString("name").substring(0, 1);
                photo.setText(fl.toUpperCase());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return convertView;
        }

    }

    //Used for fetching the current city of the user
    private void get_city() {
        lat = location.getLatitude();
        lon = location.getLongitude();
        try {
            Geocoder geocoder = new Geocoder(this);
            List<Address> addresses = null;
            addresses = geocoder.getFromLocation(lat, lon, 1);
            city = addresses.get(0).getLocality();
            Log.i("Location", "Locality: " + addresses.get(0).getLocality() + "\n Pincode" + addresses.get(0).getPostalCode() + "\n Premises" + addresses.get(0).getPremises() + "\n Admin area " + addresses.get(0).getAdminArea() + "\n Subadmin area" + addresses.get(0).getSubAdminArea() + "\n Locale" + addresses.get(0).getLocale() + "\n SubLocalility " + addresses.get(0).getSubLocality()
            );
            String add = addresses.get(0).getAddressLine(0);
            Log.i("Add", add);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        Log.i("JSON", "Reached On back pressed");
        Intent intent = new Intent(getApplicationContext(), HomeScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    //Used for check whether the device is connected to the internet or not
    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

}
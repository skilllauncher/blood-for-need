    package com.blood.blooddonation;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;

public class EmergencyRequest extends AppCompatActivity {

    ImageView aposi, anega, bnega, bposi, oposi, onega, abposi, abnega, male, female;
    int i = 1, j = 1;
    boolean sel = false, platelete = false;
    String bloodgroup;
    DatePickerDialog.OnDateSetListener dateSetListener;
    TextView location, date;
    Location location2;
    EditText name, ph;
    String location_string;
    private LocationManager locationManager;
    int STORAGE_PERMISSION_CODE = 1;
    int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    double lat;
    double lon;
    String google_map_link;
    ImageButton back;
    private String hosp_str = "hos";
    private boolean doubleBackToExitPressedOnce = false;
    private String hosp_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_request);

        aposi = findViewById(R.id.apos);
        anega = findViewById(R.id.aneg);
        bnega = findViewById(R.id.bneg);
        bposi = findViewById(R.id.bpos);
        oposi = findViewById(R.id.opos);
        onega = findViewById(R.id.oneg);
        abposi = findViewById(R.id.abpos);
        abnega = findViewById(R.id.abneg);

        //Blood group selection
        aposi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (j == 1 || sel) {
                    aposi.setImageResource(R.drawable.cgroup310);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    anega.setImageResource(R.drawable.group311);
                    bnega.setImageResource(R.drawable.group312);
                    bposi.setImageResource(R.drawable.group319);
                    oposi.setImageResource(R.drawable.group313);
                    onega.setImageResource(R.drawable.group318);
                    abposi.setImageResource(R.drawable.group317);
                    abnega.setImageResource(R.drawable.group316);

                    bloodgroup = "A+";
                } else {
                    sel = false;
                    aposi.setImageResource(R.drawable.group310);
                    j = 1;
                }
            }
        });

        anega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                anega.setImageResource(R.drawable.cgroup311);
                if (j == 1 || sel) {
                    anega.setImageResource(R.drawable.cgroup311);

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    bnega.setImageResource(R.drawable.group312);
                    bposi.setImageResource(R.drawable.group319);
                    oposi.setImageResource(R.drawable.group313);
                    onega.setImageResource(R.drawable.group318);
                    abposi.setImageResource(R.drawable.group317);
                    abnega.setImageResource(R.drawable.group316);

                    j = 0;
                    bloodgroup = "A-";
                } else {

                    sel = false;
                    anega.setImageResource(R.drawable.group311);
                    j = 1;
                }
            }
        });

        bnega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bnega.setImageResource(R.drawable.cgroup312);
                if (j == 1 || sel) {
                    bnega.setImageResource(R.drawable.cgroup312);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    anega.setImageResource(R.drawable.group311);
                    bposi.setImageResource(R.drawable.group319);
                    oposi.setImageResource(R.drawable.group313);
                    onega.setImageResource(R.drawable.group318);
                    abposi.setImageResource(R.drawable.group317);
                    abnega.setImageResource(R.drawable.group316);

                    bloodgroup = "B-";
                } else {
                    sel = false;
                    bnega.setImageResource(R.drawable.group312);
                    j = 1;
                    bloodgroup = null;
                }
            }
        });

        bposi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bposi.setImageResource(R.drawable.cgroup319);
                if (j == 1 || sel) {
                    bposi.setImageResource(R.drawable.cgroup319);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    bnega.setImageResource(R.drawable.group312);
                    anega.setImageResource(R.drawable.group311);
                    oposi.setImageResource(R.drawable.group313);
                    onega.setImageResource(R.drawable.group318);
                    abposi.setImageResource(R.drawable.group317);
                    abnega.setImageResource(R.drawable.group316);
                    bloodgroup = "B+";
                } else {
                    sel = false;
                    bposi.setImageResource(R.drawable.group319);
                    j = 1;
                    bloodgroup = null;
                }
            }
        });

        oposi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oposi.setImageResource(R.drawable.cgroup313);
                if (j == 1 || sel) {
                    oposi.setImageResource(R.drawable.cgroup313);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    bnega.setImageResource(R.drawable.group312);
                    bposi.setImageResource(R.drawable.group319);
                    anega.setImageResource(R.drawable.group311);
                    onega.setImageResource(R.drawable.group318);
                    abposi.setImageResource(R.drawable.group317);
                    abnega.setImageResource(R.drawable.group316);

                    bloodgroup = "O+";
                } else {

                    sel = false;
                    oposi.setImageResource(R.drawable.group313);
                    j = 1;
                    bloodgroup = null;
                }
            }
        });

        onega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onega.setImageResource(R.drawable.cgroup318);
                if (j == 1 || sel) {
                    onega.setImageResource(R.drawable.cgroup318);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    bnega.setImageResource(R.drawable.group312);
                    bposi.setImageResource(R.drawable.group319);
                    oposi.setImageResource(R.drawable.group313);
                    anega.setImageResource(R.drawable.group311);
                    abposi.setImageResource(R.drawable.group317);
                    abnega.setImageResource(R.drawable.group316);

                    bloodgroup = "O-";
                } else {

                    sel = false;
                    onega.setImageResource(R.drawable.group318);
                    j = 1;
                    bloodgroup = null;
                }
            }
        });

        abposi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abposi.setImageResource(R.drawable.cgroup317);
                if (j == 1 || sel) {
                    abposi.setImageResource(R.drawable.cgroup317);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    bnega.setImageResource(R.drawable.group312);
                    bposi.setImageResource(R.drawable.group319);
                    oposi.setImageResource(R.drawable.group313);
                    onega.setImageResource(R.drawable.group318);
                    anega.setImageResource(R.drawable.group311);
                    abnega.setImageResource(R.drawable.group316);

                    bloodgroup = "AB+";
                } else {

                    sel = false;
                    abposi.setImageResource(R.drawable.group317);
                    j = 1;
                    bloodgroup = null;
                }
            }
        });

        abnega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abnega.setImageResource(R.drawable.cgroup316);
                if (j == 1 || sel) {
                    abnega.setImageResource(R.drawable.cgroup316);
                    j = 0;

                    sel = true;
                    //Set all other to blank
                    aposi.setImageResource(R.drawable.group310);
                    bnega.setImageResource(R.drawable.group312);
                    bposi.setImageResource(R.drawable.group319);
                    oposi.setImageResource(R.drawable.group313);
                    onega.setImageResource(R.drawable.group318);
                    abposi.setImageResource(R.drawable.group317);
                    anega.setImageResource(R.drawable.group311);

                    bloodgroup = "AB-";
                } else {
                    sel = false;
                    abnega.setImageResource(R.drawable.group316);
                    j = 1;
                }
            }
        });

        //For checking whether the required permissions are granted by the user or not
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(EmergencyRequest.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

        }
        location2 = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        location = findViewById(R.id.are);
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                get_city();
                location_string = location.getText().toString();
            }
        });

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EmergencyRequest.this, LoginScreen.class);
                startActivity(intent);
            }
        });

        date = findViewById(R.id.dat);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("JSON", "Reached date");
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int date = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(EmergencyRequest.this, android.R.style.Theme_Holo_Dialog_NoActionBar_MinWidth, dateSetListener, year, month, date);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;

                String dat = year + "/" + month + "/" + dayOfMonth;
                date.setText(dat);
            }
        };


        name = findViewById(R.id.nam);
        ph = findViewById(R.id.ph);

        final CheckBox checkBox = findViewById(R.id.platelet);


        Button submit = findViewById(R.id.sub);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (haveNetworkConnection()) {
                    platelete = checkBox.isChecked();

                    String name_str = name.getText().toString();


                    request(location.getText().toString(), bloodgroup, name_str, location_string, hosp_str, ph.getText().toString());
                } else
                    noNetworkAction();
            }
        });

        //Autocomplete Place Retrieval using Google Place API
        Places.initialize(getApplicationContext(), "AIzaSyC1u7yPHGjEvVUPFpCo6P_aBOrBNDlJKIk");
        // Initialize the AutocompleteSupportFragment.
        final AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS));

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i("Add", "Place: " + place.getName() + ", " + place.getId());
                Log.i("JSON", "Hospital address " + place.getAddress());
                hosp_str = place.getName() + " " + place.getAddress();
                hosp_name = place.getName();

            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("Add", "An error occurred: " + status);
            }
        });


    }

    //For getting the current latlng position of the user and infuse it with the google maps link for navigation purpose
    private void get_city() {
        if(location2!=null) {
            lat = location2.getLatitude();
            lon = location2.getLongitude();
            try {
                Log.i("JSON", "Lat Lon" + lat + " " + lon);
                Geocoder geocoder = new Geocoder(this);
                List<Address> addresses = null;
                addresses = geocoder.getFromLocation(lat, lon, 1);
                String cit = addresses.get(0).getLocality();
                location.setText(cit);

                google_map_link = "https://www.google.com/maps?q=" + lat + "," + lon;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            Log.i("JSON", "LOcation problem");
            Toast.makeText(this, "Location Not found, please enter hospital address.", Toast.LENGTH_SHORT).show();
        }

    }

    //For sending the user data to the server
    private void request(String city, final String bloodgroup, final String name, final String location, final String hospital, final String phonenum) {

        final RequestParams params = new RequestParams();

        params.put("name", name);
        params.put("whatsapp", "null");
        params.put("mobile", phonenum);
        params.put("relation", "Emergency");
        params.put("needed_by", "skdafjhk");
        params.put("bloodgrp", bloodgroup);
        params.put("city", city);
        params.put("area", hosp_str);
        params.put("google_loc", google_map_link);
        params.put("status", "OPEN");
        params.put("hospital", hosp_name);
        params.put("date", date.getText());
        params.put("platelets", platelete);
        params.put("req_type", "EMERGENCY");

        final String fbloodgroup = bloodgroup;

        String request_url = "https://bloodtransfer.herokuapp.com/index.php/data/reciever";
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(request_url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i("JSON", "JSON is " + response.toString());
                Log.i("JSON", "Status  code" + statusCode);
                Log.i("JSON", response.toString());
                try {
                    Log.i("JSON", response.getString("message"));
                    if (response.getString("message").equals("No donors found with your preferences")) {
                        Toast.makeText(getApplicationContext(), "Sorry No Donors Available", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Toast.makeText(getApplicationContext(), "Request Success :)", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplicationContext(), FilteredResult.class);
                intent.putExtra("json", response.toString());
                intent.putExtra("rname", name);
                intent.putExtra("blood", fbloodgroup);
                intent.putExtra("hospital", hosp_str);
                intent.putExtra("location", location_string);
                intent.putExtra("google_loc", google_map_link);
                intent.putExtra("mobile", phonenum);
                intent.putExtra("type", "EMERGENCY");

                startActivity(intent);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                Log.i("JSON", "Status code" + statusCode);
                Log.i("JSON", errorResponse.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "Request Success :)", Toast.LENGTH_SHORT).show();
                Log.i("JSON", "Status  code" + statusCode);
                Log.i("JSON", responseString);


                Toast.makeText(getApplicationContext(), "Request Success :)", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplicationContext(), FilteredResult.class);
                intent.putExtra("json", responseString);
                intent.putExtra("rname", name);
                intent.putExtra("blood", fbloodgroup);
                intent.putExtra("hospital", hospital);
                intent.putExtra("location", location_string);
                intent.putExtra("google_loc", google_map_link);
                intent.putExtra("mobile", phonenum);

                startActivity(intent);
            }
        });

        donors();
    }

    //Displaying all the donors
    private void donors() {
        String displayurl = "https://bloodtransfer.herokuapp.com/index.php/data/searchall";

        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();

        client.post(displayurl, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i("JSON", "JSON is " + response.toString());
                Log.i("JSON", "Status  code" + statusCode);
                Log.i("JSON", response.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                Log.i("JSON", "Status code" + statusCode);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i("JSON", "Status  code" + statusCode);
                Log.i("JSON", responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                Log.i("JSON", response.toString());
                Log.i("JSON", "Length " + response.length());

            }
        });
    }


    //Double back button to exit
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(this, LoginScreen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    //COde to check whether the device is connected to the internet or not
    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    //Snackbar which displays not internet connection and try again button
    private void noNetworkAction() {
        final Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                "No internet connection.",
                Snackbar.LENGTH_INDEFINITE);
        snackbar.setActionTextColor(ContextCompat.getColor(Objects.requireNonNull(getApplicationContext()),
                R.color.black));
        snackbar.setAction(R.string.try_again, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //recheck internet connection and call DownloadJson if there is internet
                if (!haveNetworkConnection())
                    noNetworkAction();
                else {
                    Intent intent = getIntent();
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    finish();
                    startActivity(intent);
                }
            }
        }).show();
    }
}
package com.blood.blooddonation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class FeedBack extends AppCompatActivity {

    EditText name,ph,email,message;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);

        name = findViewById(R.id.nm);
        ph = findViewById(R.id.nma);
        email = findViewById(R.id.ema);
        message = findViewById(R.id.mess);
        submit = findViewById(R.id.button);


        //Back button intent
        ImageButton back = findViewById(R.id.feedb);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });

        //Submit button
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit.setText("Feed back sending");
                String name_str = name.getText().toString();
                String ph_str = ph.getText().toString();
                String email_str = email.getText().toString();
                String message_str = message.getText().toString();

                send(name_str,ph_str,email_str,message_str);
            }
        });


    }


    //Code for sending the feedback to the database
    private void send(String name_str, String ph_str, String email_str, String message_str) {
        String feedback_url = "https://bloodtransfer.herokuapp.com/index.php/data/feedback";
        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("name",name_str);
        params.put("phone",ph_str);
        params.put("email",email_str);
        params.put("message",message_str);

        client.post(feedback_url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i("JSON", "JSON is " + response.toString());
                Toast.makeText(getApplicationContext(),"Feedback sent Succesfully",Toast.LENGTH_LONG).show();
                submit.setText("Feedback Sent");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.i("JSON", "Status code" + statusCode);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "Request Success:)", Toast.LENGTH_SHORT).show();
                Log.i("JSON", responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                Log.i("JSON", response.toString());
                Toast.makeText(getApplicationContext(),"Feedback sent Succesfully",Toast.LENGTH_LONG).show();
                submit.setText("Feedback Sent");
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(),HomeScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);

    }
}

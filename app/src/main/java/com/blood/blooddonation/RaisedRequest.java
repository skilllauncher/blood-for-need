package com.blood.blooddonation;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.HttpClient;


//Class mainly used to fetch the details of the user that are raised by the requestor.
public class RaisedRequest extends AppCompatActivity {

    String email, profileid, request_id, rec_name;
    JSONArray result;
    DatePickerDialog.OnDateSetListener dateSetListener;
    private String mod_date = "2018/05/21";
    double lat;
    double lon;
    Location location;
    private LocationManager locationManager;
    private int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_raised_request);

        ImageButton back = findViewById(R.id.back_donor);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });

        if (haveNetworkConnection()) {
            email = getIntent().getStringExtra("email");

            profile();
        } else {
            LottieAnimationView nointernet = findViewById(R.id.no_internet);
            nointernet.setVisibility(View.VISIBLE);
        }
    }


    //Function used to append all requested donor details on the cardview.
    private void getRequests() {

        Log.i("JSON", profileid);

        final String requesturl = "https://bloodtransfer.herokuapp.com/index.php/data/getraisedreq";

        RequestParams params = new RequestParams();
        params.put("id", profileid);

        AsyncHttpClient client = new AsyncHttpClient();
        client.post(requesturl, params, new

                JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Log.i("JSON", "JSON is " + response.toString());
                        Log.i("JSON", "Status  code" + statusCode);
                        Log.i("JSON", response.toString());
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject
                            errorResponse) {
                        Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                        Log.i("JSON", "Status code" + statusCode);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable
                            throwable) {
                        Log.i("JSON", "Status  code" + statusCode);
                        Log.i("JSON", responseString);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        Log.i("JSON", response.toString());
                        Log.i("JSON", "Length" + response.length());


                        ListView listview = findViewById(R.id.listview1);

                        try {
                            if (response.getString(0).equals("You have no requests")) {
                                Log.i("JSON", "No requests raised");
                                LottieAnimationView no_raised = findViewById(R.id.no_raised);
                                no_raised.setVisibility(View.VISIBLE);

                                TextView text = findViewById(R.id.text);
                                text.setVisibility(View.VISIBLE);

                            } else if (response.length() > 0) {
                                result = response;
                                CustomAdapter adapter = new CustomAdapter();
                                listview.setAdapter(adapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

    }

    private void profile() {
        final String profileurl = "https://bloodtransfer.herokuapp.com/index.php/data/profile";


        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("email", email);

        client.post(profileurl, params, new

                JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Log.i("profile", "JSON is " + response.toString());
                        Log.i("profile", "Status  code" + statusCode);
                        Log.i("profile", response.toString());
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject
                            errorResponse) {
                        Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                        Log.i("profile", "Status code" + statusCode);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable
                            throwable) {
                        Log.i("profile", "Status  code" + statusCode);
                        Log.i("profile", responseString);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        Log.i("profile", response.toString());
                        Log.i("profile", "Length" + response.length());
                        try {
                            profileid = response.getJSONObject(0).getString("ID");
                            rec_name = response.getJSONObject(0).getString("name");

                            getRequests();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
    }

    class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return result.length();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.raisedcardview, null);


            ImageView dir = convertView.findViewById(R.id.dir_raised);
            final TextView rec_name = convertView.findViewById(R.id.rename);
            final TextView donr_name = convertView.findViewById(R.id.dnrnm);
            TextView blood = convertView.findViewById(R.id.blood_group_card_1);
            TextView last_d = convertView.findViewById(R.id.ldtd);
            Button contact = convertView.findViewById(R.id.resend);
            Button statu = convertView.findViewById(R.id.status);

            final ImageButton menu = convertView.findViewById(R.id.imageButton);
            menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPopupMenu(menu, position);
                }
            });


            try {
                final JSONArray array = result.getJSONArray(position);

                request_id = result.getJSONArray(position).getJSONObject(0).getString("request_id");


                donr_name.setText(array.getJSONObject(0).getString("donorname"));
                rec_name.setText(array.getJSONObject(0).getString("patientname"));
                blood.setText(array.getJSONObject(0).getString("bloodgroup"));
                final String mobile = array.getJSONObject(0).getString("contact_no");
                last_d.setText(array.getJSONObject(0).getString("date"));
                statu.setText(array.getJSONObject(0).getString("req_status"));

                dir.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        if (ActivityCompat.checkSelfPermission(RaisedRequest.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(RaisedRequest.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            ActivityCompat.requestPermissions(RaisedRequest.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                            return;
                        }
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                        lat = location.getLatitude();
                        lon = location.getLongitude();

                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                                Uri.parse("http://maps.google.com/maps?saddr=" + lat + "," + lon + "&daddr=" + 17.6868 + "," + 83.2185));
                        startActivity(intent);
                    }
                });
                contact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getApplicationContext(), "Sending message", Toast.LENGTH_SHORT).show();
                        String message = "Hello " + donr_name.getText().toString() + "\n Please accept or reject the blood request requested by Mr/Mrs. " + rec_name + "\n\n";
                        String link1 = "https://bloodtransfer.herokuapp.com/index.php/Oauth/accepted/" + rec_name.getText().toString() + "/" + mobile, link2 = "https://bloodtransfer.herokuapp.com/index.php/Oauth/rejected" + rec_name.getText().toString() + "/" + mobile;
                        link1 = link1.replaceAll(" ", "");
                        link2 = link2.replaceAll(" ", "");

                        message = message + "\n" + link1 + "\n" + link2;

                        sendsms(message, mobile, donr_name.getText().toString());
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return convertView;
        }

        private void showPopupMenu(View view, int position) {
            // inflate menu
            PopupMenu popup = new PopupMenu(view.getContext(), view);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.raisedcardmenu, popup.getMenu());
            popup.setOnMenuItemClickListener(new MyMenuItemClickListener(position));
            popup.show();
        }

        private void sendsms(String message, String number, final String donor_name) {

            Log.i("JSON", "The donor number is :" + number);

            String otp_url = "https://bloodtransfer.herokuapp.com/index.php/Oauth/resendhelpsms";
            Log.i("JSON", otp_url);

            RequestParams params = new RequestParams();
            params.put("mobile", number);
            params.put("msg", message);

            Log.i("JSON", message);

            AsyncHttpClient client = new AsyncHttpClient();
            client.post(otp_url, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.i("JSON", "JSON is " + response.toString());
                    Log.i("JSON", "Status  code" + statusCode);
                    Log.i("JSON", "Response is " + response.toString());
                    Toast.makeText(getApplicationContext(), "Message Sent to " + donor_name, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                    Log.i("JSON", "Status code" + statusCode);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toast.makeText(getApplicationContext(), "Request Success :)", Toast.LENGTH_SHORT).show();
                    Log.i("JSON", "Status  code" + statusCode);
                    Log.i("JSON", "Response is " + responseString);
                    Toast.makeText(getApplicationContext(), "Message sent to " + donor_name, Toast.LENGTH_SHORT).show();

                }
            });

        }

    }

    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;

        public MyMenuItemClickListener(int positon) {
            this.position = positon;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.Not_interasted_catugury:
                    closeRequest(position);
                    break;

                case R.id.No_interasted:
                    Log.i("JSON", "Re confirm");

                    updateDate(position);

                    break;
                default:
            }
            return false;
        }
    }


    //Function used to close the request made by the requestor if fulfilled.
    private void closeRequest(int position) {
        RequestParams params = new RequestParams();

        Log.i("JSON", request_id);
        params.put("id", request_id);
        params.put("status", "FULLFILLED");

        String closeurl = "https://bloodtransfer.herokuapp.com/index.php/data/updatereqstatus";

        AsyncHttpClient client = new AsyncHttpClient();
        client.post(closeurl, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i("JSON", "JSON is " + response.toString());
                Log.i("JSON", "Status  code" + statusCode);
                Log.i("JSON", "Response is " + response.toString());
                Toast.makeText(getApplicationContext(), "Request Closed", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                Log.i("JSON", "Status code" + statusCode);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "Request Success :)", Toast.LENGTH_SHORT).show();
                Log.i("JSON", "Status  code" + statusCode);
                Log.i("JSON", "Response is " + responseString);
                Toast.makeText(getApplicationContext(), "Request Closed", Toast.LENGTH_SHORT).show();

            }
        });

    }

    //Function used to update the date
    private void updateDate(int position) {

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int date = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(RaisedRequest.this, android.R.style.Theme_Holo_Dialog_NoActionBar_MinWidth, dateSetListener, year, month, date);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                final String dat = year + "/" + month + "/" + dayOfMonth;

                Log.i("JSON", dat);

                RequestParams params = new RequestParams();
                Log.i("JSON", "the reques is " + request_id);
                params.put("id", request_id);
                params.put("date", dat);

                String dateurl = "https://bloodtransfer.herokuapp.com/index.php/data/updatereqdate";
                Log.i("JSON", "Clicked");

                AsyncHttpClient client = new AsyncHttpClient();
                client.post(dateurl, params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Log.i("JSON", "JSON is " + response.toString());
                        Log.i("JSON", "Status  code" + statusCode);
                        Log.i("JSON", "Response is " + response.toString());
                        Toast.makeText(getApplicationContext(), "Date Changed", Toast.LENGTH_SHORT).show();
                        mod_date = (dat);

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                        Log.i("JSON", "Status code" + statusCode);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Log.i("JSON", "Status  code" + statusCode);
                        Log.i("JSON", "Response is " + responseString);
                        Toast.makeText(getApplicationContext(), "Date Changed", Toast.LENGTH_SHORT).show();
                        mod_date = (dat);

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        Log.i("JSON", "Reached Success");
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        Log.i("JSON", "Reached Failed");
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        Log.i("JSON", "Reached Success");
                    }
                });
            }


        };

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RaisedRequest.this, HomeScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }


    //Function used to check whether the device is connected to the internet or not
    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

}
package com.blood.blooddonation;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.goodiebag.pinview.Pinview;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity implements SMSReceiver.OTPReceiveListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int RESOLVE_HINT = 123;
    Button getotp, verify;
    EditText phone;
    TextView resen;
    Pinview pinv;
    CardView cardView;
    private String otp_pin = "";
    ImageView blur;
    boolean otp_ver, sign_status = false;
    String email;
    private SMSReceiver smsReceiver;
    private String personName;
    GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        otp_ver = false;
        phone = findViewById(R.id.phonenm);
        verify = findViewById(R.id.verifysub);
        pinv = findViewById(R.id.pincode);
        cardView = findViewById(R.id.card);
        blur = findViewById(R.id.imageView4);
        getotp = findViewById(R.id.getotp);
        resen = findViewById(R.id.resend);

        mGoogleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                .addConnectionCallbacks(MainActivity.this)
                .enableAutoManage(MainActivity.this,MainActivity.this)
                .addApi(Auth.CREDENTIALS_API)
                .build();

        ImageButton imageButton = findViewById(R.id.main_back);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "You cannot go back at this stage", Toast.LENGTH_SHORT).show();
            }
        });

        getotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (haveNetworkConnection()) {
                    sendVerificationCode();
                    cardView.setVisibility(View.VISIBLE);
                    blur.setVisibility(View.VISIBLE);
                    getotp.requestFocus();
                    phone.clearFocus();
                    getotp.setCursorVisible(true);
                    getotp.setVisibility(View.GONE);
                } else {
                    noNetworkAction();
                }
            }
        });

        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (haveNetworkConnection()) {
                    verifySignInCode();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(pinv.getWindowToken(), 0);
                } else {
                    noNetworkAction();
                }
            }
        });

        resen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (haveNetworkConnection())
                    resendVerificationCode(phone.getText().toString());
                else
                    noNetworkAction();
            }
        });

        getFbInfo();

        AppSignatureHashHelper appSignatureHashHelper = new AppSignatureHashHelper(MainActivity.this);

        Log.i("JSON", "Apps Hash Key: " + appSignatureHashHelper.getAppSignatures().get(0));

    }

    //Code used for sending the verification code if asked by the user again
    private void resendVerificationCode(String number) {
        RequestParams params = new RequestParams();
        params.put("mobile", number);

        Log.i("JSON", "no:" + number);
        String otp_url = "https://bloodtransfer.herokuapp.com/index.php/Oauth/resend";

        AsyncHttpClient client = new AsyncHttpClient();
        client.post(otp_url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i("JSON", "JSON is " + response.toString());
                Log.i("JSON", "Status  code" + statusCode);
                Log.i("JSON", response.toString());
                otp_pin = response.toString().substring(0, 6);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                Log.i("JSON", "Status code" + statusCode);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                Log.i("JSON", "Status  code" + statusCode);
                Log.i("JSON", responseString);
                otp_pin = responseString.substring(0, 6);
            }
        });

    }

    //Code used to verify whether the OTP entered by the user is valid or not
    public void verifySignInCode() {
        String code = pinv.getValue();

        Log.i("JSON", "pinv = " + code + " otp_pin = " + otp_pin);
        if (otp_pin.equals(code) && code != null) {
            Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
            pinv.clearFocus();
            phone.clearFocus();
            profile();
        } else {
            Toast.makeText(getApplicationContext(), "Wrong otp :(", Toast.LENGTH_SHORT).show();
        }
    }


    //
    private void createUser() {
        if (haveNetworkConnection()) {
            GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(MainActivity.this);
            final RequestParams params = new RequestParams();


            String user_url = "https://bloodtransfer.herokuapp.com/index.php/data/save";

            if (account != null) {
                personName = account.getDisplayName();

                params.put("name", personName);
                Log.i("JSON", "Create mohbile" + phone.getText().toString());
                params.put("mobile", phone.getText().toString());

                params.put("gender", "Gender");
                params.put("email", account.getEmail());
                params.put("city", "City");

                params.put("area", "area");
                params.put("lastdonated", "date");

                AsyncHttpClient client = new AsyncHttpClient();
                client.post(user_url, params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Log.i("JSON", "JSON is " + response.toString());
                        Log.i("JSON", "Status  code" + statusCode);
                        Log.i("JSON", response.toString());
                        finish();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                        Log.i("JSON", "Status code" + statusCode);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Toast.makeText(getApplicationContext(), "Request Success:)", Toast.LENGTH_SHORT).show();
                        Log.i("JSON", "Status  code" + statusCode);
                        Log.i("JSON", responseString);
                        finish();
                    }
                });

            } else {
                GraphRequest request = GraphRequest.newMeRequest(
                        AccessToken.getCurrentAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            private String personalemail;

                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                try {

                                    if (object.has("email")) {
                                        personalemail = object.getString("email");
                                        personName = object.getString("first_name") + object.getString("last_name");
//                                    Log.i("JSON", name + " " + personalemail + " " + gender + " " + bloodgroup + " " + visibility_str + " " + age.getText().toString() + " " + phone.getText().toString() + " " + dat);
                                        params.put("name", personName);
                                        params.put("mobile", phone.getText().toString());
                                        params.put("donor", "no");
                                        params.put("visibility", true);
                                        params.put("gender", "male");
                                        params.put("email", personalemail);
                                        params.put("age", "");
                                        params.put("city", "");
                                        params.put("bloodgrp", "");
                                        params.put("area", "area");
                                        params.put("lastdonated", "");

                                        Log.i("JSON", "Has email" + personalemail);
                                        String user_url = "https://bloodtransfer.herokuapp.com/index.php/data/save";

                                        AsyncHttpClient client = new AsyncHttpClient();
                                        client.post(user_url, params, new JsonHttpResponseHandler() {
                                            @Override
                                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                                Log.i("JSON", "JSON is " + response.toString());
                                                Log.i("JSON", "Status  code" + statusCode);
                                                Log.i("JSON", response.toString());

                                            }

                                            @Override
                                            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                                                Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                                                Log.i("JSON", "Status code" + statusCode);
                                            }

                                            @Override
                                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                                Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                                                Log.i("JSON", "Status  code" + statusCode);
                                                Log.i("JSON", responseString);
                                                finish();
                                            }
                                        });
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "error in name", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email,gender,birthday"); // id,first_name,last_name,email,gender,birthday,cover,picture.type(large)
                request.setParameters(parameters);
                request.executeAsync();
            }
        } else {
            noNetworkAction();
        }
    }

    //Used for requesting an OTP
    private void sendVerificationCode() {
        String number = phone.getText().toString();

        RequestParams params = new RequestParams();
        params.put("mobile", number);

        Log.i("JSON", "no:" + number);
        // Oauth/sendsms/(message)/(phno)/
        String otp_url = "https://bloodtransfer.herokuapp.com/index.php/Oauth/otp";

        try {
            smsReceiver = new SMSReceiver();
            smsReceiver.setOTPListener(MainActivity.this);

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
            this.registerReceiver(smsReceiver, intentFilter);

            SmsRetrieverClient client = SmsRetriever.getClient(MainActivity.this);

            Task<Void> task = client.startSmsRetriever();
            task.addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    // API successfully started
                    Log.i("JSON", "Task success");
                }
            });

            task.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    // Fail to start API
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        AsyncHttpClient client = new AsyncHttpClient();
        client.post(otp_url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i("JSON", "JSON is " + response.toString());
                Log.i("JSON", "Status  code" + statusCode);
                Log.i("JSON", response.toString());
                otp_pin = response.toString().substring(0, 6);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                Log.i("JSON", "Status code" + statusCode);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "Request Success :)", Toast.LENGTH_SHORT).show();
                Log.i("JSON", "Status  code" + statusCode);
                Log.i("JSON", responseString);
                otp_pin = responseString.substring(0, 6);
            }
        });
    }

    // Construct a request for phone numbers and show the picker
    private void requestHint() {
        HintRequest hintRequest = new HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build();

        PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(mGoogleApiClient, hintRequest);
        try {
            startIntentSenderForResult(intent.getIntentSender(), RESOLVE_HINT, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    //Obtain the phone number from the result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESOLVE_HINT) {
            if (resultCode == RESULT_OK) {
                Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
                String number = credential.getId().toString();

                number = number.substring(3);
                Log.i("JSON", number);
                phone.setText(number);
                // credential.getId();  <-- will need to process phone number string
            } else if (resultCode == RESULT_CANCELED) {
                Log.i("JSON", "Failed");
            } else {
                Log.i("JSON", "Unknown");
            }
        }
    }


    @Override
    public void onBackPressed() {
        Toast.makeText(getApplicationContext(), "You cant go back at this stage", Toast.LENGTH_SHORT).show();
    }


    //Used to trim all the unwanted data from the OTP message provided by the server
    @Override
    public void onOTPReceived(String otp) {
        showToast("OTP Received: " + otp);
        Pattern p = Pattern.compile("(|^)\\d{6}");
        if (otp != null) {
            Matcher m = p.matcher(otp);
            if (m.find()) {
                pinv.setValue(m.group(0));
                verifySignInCode();
            }

        }

        if (smsReceiver != null) {
            LocalBroadcastManager.getInstance(MainActivity.this).unregisterReceiver(smsReceiver);
        }
    }

    @Override
    public void onOTPTimeOut() {
        showToast("OTP Time out");
        Log.i("JSON", "Time out");
    }

    @Override
    public void onOTPReceivedError(String error) {
        showToast(error);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (smsReceiver != null) {
            LocalBroadcastManager.getInstance(MainActivity.this).unregisterReceiver(smsReceiver);
        }
    }


    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Log.i("JSON", msg);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void profile() {
        String profileurl = "https://bloodtransfer.herokuapp.com/index.php/data/profile";

        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
        if (acct != null) {
            email = acct.getEmail();
        }

        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("email", email);

        client.post(profileurl, params, new

                JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Log.i("JSON", "JSON is " + response.toString());
                        Log.i("JSON", "Status  code" + statusCode);
                        Log.i("JSON", response.toString());
                        sign_status = response.length() > 0;
                        Log.i("JSON", String.valueOf(sign_status));

                        Log.i("JSON", String.valueOf(sign_status) + " in Success method");
                        if (!sign_status) {
                            createUser();
                        }
                        Intent intent = new Intent(getApplicationContext(), AskUser.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject
                            errorResponse) {
                        Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                        Log.i("JSON", "Status code" + statusCode);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable
                            throwable) {
                        Log.i("JSON", "Status  code" + statusCode);
                        Log.i("JSON", responseString);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        Log.i("JSON", response.toString());
                        Log.i("JSON", "Length" + response.length());
                        sign_status = response.length() > 0;
                        Log.i("JSON", String.valueOf(sign_status));

                        Log.i("JSON", String.valueOf(sign_status) + " in Success method");
                        if (!sign_status) {
                            createUser();
                        }
                        Intent intent = new Intent(getApplicationContext(), AskUser.class);
                        startActivity(intent);
                    }
                });
    }


    private void getFbInfo() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();

        if (isLoggedIn) {
            GraphRequest request = GraphRequest.newMeRequest(
                    AccessToken.getCurrentAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            try {
//                            Log.d(LOG_TAG, "fb json object: " + object);
//                            Log.d(LOG_TAG, "fb graph response: " + response);

                                String first_name = object.getString("first_name");
                                String last_name = object.getString("last_name");
//                            String gender = object.getString("gender");
//                            String birthday = object.getString("birthday");
//                            String image_url = "http://graph.facebook.com/" + id + "/picture?type=large";

                                if (object.has("email")) {
                                    email = object.getString("email");
                                }
                                Log.i("JSON", "FB email is " + email);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "error in name", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,first_name,last_name,email,gender,birthday"); // id,first_name,last_name,email,gender,birthday,cover,picture.type(large)
            request.setParameters(parameters);
            request.executeAsync();
        }
    }

    //Used to check whether the device is connected to the internet or not
    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    //Snackbar code used to display custom text and button based on the certain result
    private void noNetworkAction() {
        final Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                "No internet connection.",
                Snackbar.LENGTH_INDEFINITE);
        snackbar.setActionTextColor(ContextCompat.getColor(Objects.requireNonNull(getApplicationContext()),
                R.color.black));
        snackbar.setAction(R.string.try_again, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //recheck internet connection and call DownloadJson if there is internet
                if (!haveNetworkConnection())
                    noNetworkAction();
                else {
                    Intent intent = getIntent();
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    finish();
                    startActivity(intent);
                }
            }
        }).show();
    }

}

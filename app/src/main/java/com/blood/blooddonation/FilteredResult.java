package com.blood.blooddonation;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

public class FilteredResult extends AppCompatActivity {

    JSONArray result;
    ListView listView;
    String rname, blood_grp, hospital, location, goog_loc, mobile, profile_id = "123", request_id;

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), HomeScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtered_result);

        String json = getIntent().getStringExtra("json");
        rname = getIntent().getStringExtra("rname");
        blood_grp = getIntent().getStringExtra("blood");
        hospital = getIntent().getStringExtra("hospital");
        location = getIntent().getStringExtra("location");
        goog_loc = getIntent().getStringExtra("google_loc");
        mobile = getIntent().getStringExtra("mobile");
        profile_id = getIntent().getStringExtra("profile_id");
        request_id = getIntent().getStringExtra("request_id");

        Log.i("request_id", "Request id is " + request_id);
        Log.i("JSON", "Recieved mobile is " + mobile);

        //Filtering all the donors based on the blood group and city
        try {
            if (json.contains("No donors found with your preferences")) {
                LottieAnimationView animationView = findViewById(R.id.no_donors);
                animationView.setVisibility(View.VISIBLE);
                TextView text = findViewById(R.id.don_text);
                text.setVisibility(View.VISIBLE);
            } else {
                Log.i("filtered", json);
                result = new JSONArray(json);
                Log.i("filtered", String.valueOf(result.length()));

                listView = findViewById(R.id.listview_filter);
                CustomAdapter adapter = new CustomAdapter();
                listView.setAdapter(adapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    //USed for displaying the resultant data on the cardview
    class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return result.length();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.cardview, null);

            TextView name = convertView.findViewById(R.id.name_card);
            TextView place = convertView.findViewById(R.id.place);
            TextView blood = convertView.findViewById(R.id.blood_group_card);
            TextView last_d = convertView.findViewById(R.id.last_donated_card);
            final Button contact = convertView.findViewById(R.id.help);

            final String don_id, don_phone;
            try {
                final JSONObject object = result.getJSONObject(position);
                name.setText(object.getString("name"));
                place.setText(object.getString("city"));
                blood.setText(object.getString("bloodgroup"));
                last_d.setText("Last donated on " + object.getString("last_donated"));

                final String visibility;
                visibility = object.getString("visiblity");
                Log.i("JSON",visibility);

                don_id = object.getString("ID");
                don_phone = object.getString("mobile");

                if (visibility.equals("1"))
                    contact.setText("Call now ");

                contact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (visibility.equals("1")) {
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            intent.setData(Uri.parse("tel:" + don_phone));
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "Sending message", Toast.LENGTH_SHORT).show();
                            String message = null;
                            String don_name = null;
                            try {
                                String loc_link = "https://maps.google.com/?q=" + goog_loc;
                                String link1 = "https://bloodtransfer.herokuapp.com/index.php/Oauth/accepted/" + rname + "/" +
                                        mobile, link2 = "https://bloodtransfer.herokuapp.com/index.php/Oauth/rejected" + rname + "/" + mobile;
                                link1 = link1.replaceAll(" ", "");
                                link2 = link2.replaceAll(" ", "");
                                don_name = object.getString("name");

                                message = "Hello " + object.getString("name") + ",\n\n Will you be able to donate blood for a patient who needs blood" + ".\nHere are the details:-\n\n Name: " + rname + "\n Mobile number: " + mobile +
                                        "\n Hospital place: " + hospital + "\n Blood group needed: " + blood_grp + "\n\n Location link: " + loc_link + "\n\nClick on this link to accept this request\n" + link1 + "\n\n Click here to reject\n" + link2;

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            Log.i("JSON", mobile);
                            sendsms(message, don_phone, don_name, don_id);
                        }
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return convertView;
        }


        //Used for sending SMS to the donor
        private void sendsms(String message, String number, final String don_name, String don_id) {

            Log.i("JSON", "The donor number is :" + number);

            String otp_url = "https://bloodtransfer.herokuapp.com/index.php/Oauth/askhelp";
            Log.i("JSON", otp_url);

            Log.i("JSON", "SMS " + don_id + " " + don_id + " rec_id " + profile_id + " don_id " + don_id + " req_id: " + request_id
            );
            RequestParams params = new RequestParams();
            params.put("mobile", number);
            params.put("msg", message);
            params.put("requester_id", profile_id);
            params.put("donorid", don_id);
            params.put("request_id", request_id);


            Log.i("JSON", message);

            Log.i("JSON", "reciever id :" + profile_id + "\n donorid: " + don_id + " \nreques_id" + request_id);

            AsyncHttpClient client = new AsyncHttpClient();
            client.post(otp_url, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.i("JSON", "JSON is " + response.toString());
                    Log.i("JSON", "Status  code" + statusCode);
                    Log.i("JSON", "Response is " + response.toString());
                    Toast.makeText(getApplicationContext(), "Message Sent to " + don_name, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                    Log.i("JSON", "Status code" + statusCode);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toast.makeText(getApplicationContext(), "Request Success :)", Toast.LENGTH_SHORT).show();
                    Log.i("JSON", "Status  code" + statusCode);
                    Log.i("JSON", "Response is " + responseString);
                    Toast.makeText(getApplicationContext(), "Message sent to " + don_name, Toast.LENGTH_SHORT).show();

                }
            });

        }

    }

}

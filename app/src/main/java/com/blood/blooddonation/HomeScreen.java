package com.blood.blooddonation;


import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnSuccessListener;
import com.google.android.play.core.tasks.Task;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;

import static com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE;

public class HomeScreen extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TextView usrnm;
    String email;
    private TextView propic;
    boolean doubleBackToExitPressedOnce = false;
    DatePickerDialog.OnDateSetListener dateSetListener;
    Switch toggle_switch;
    RadioGroup rg;
    String update;
    AppUpdateManager appUpdateManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        Toolbar toolbar = findViewById(R.id.toolbar_home);
        setSupportActionBar(toolbar);

        update = getIntent().getStringExtra("update");

        if (update != null) {
            updatedialog();
        }

//        setTaskBarColored(HomeScreen.this);

        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ham, getApplicationContext().getTheme());

        //Code for drawer layout
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.Open, R.string.Close);
        drawer.addDrawerListener(toggle);
        toggle.setHomeAsUpIndicator(drawable);

        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));

        toggle.syncState();


        navigationView.setNavigationItemSelectedListener(this);


        //Intent for RequestBlood.class
        Button alldonors = findViewById(R.id.find_donor_button);
        alldonors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RequestBlood.class);
                startActivity(intent);
            }
        });

        //Intent for RequestActivity.class
        Button request = findViewById(R.id.see_request_button);
        request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RequestActivity.class);
                startActivity(intent);
            }
        });

        View n = navigationView.getHeaderView(0);

//Initialization of views in navigation bar.
        usrnm = n.findViewById(R.id.userName);
        propic = n.findViewById(R.id.profilePic);

        //Used for fetching the username and email id if the user is logged in using the google
        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
        if (acct != null) {
            String name = acct.getGivenName();
            email = acct.getEmail();

            assert name != null;
            String cap = name.substring(0, 1).toUpperCase() + name.substring(1);
            Log.i("name_of_acct", name);

            propic.setText(String.valueOf(cap.charAt(0)));
            usrnm.setText(cap);
        } else {
            getFbInfo();
        }

        //Used for updating the last donation date
        final TextView update = findViewById(R.id.last_donated_update);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int date = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(HomeScreen.this, android.R.style.Theme_Holo_Dialog_NoActionBar_MinWidth, dateSetListener, year, month, date);
                Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        //Date listener used for picking up the date month and year
        dateSetListener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;

                String dat = year + "/" + month + "/" + dayOfMonth;

                update.setText("Recent donation date updated");
                rg.setVisibility(View.INVISIBLE);


                RequestParams params = new RequestParams();
                params.put("email", email);
                params.put("lastdonated", dat);


                AsyncHttpClient client = new AsyncHttpClient();

                String lddupdateurl = "https://bloodtransfer.herokuapp.com/index.php/data/lastdonatedupdate";

                client.post(lddupdateurl, params, new

                        JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                Log.i("JSON", "JSON is " + response.toString());
                                Log.i("JSON", "Status  code" + statusCode);
                                Log.i("JSON", response.toString());
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject
                                    errorResponse) {
                                Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                                Log.i("JSON", "Status code" + statusCode);
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable
                                    throwable) {
                                Log.i("JSON", "Status  code" + statusCode);
                                Log.i("JSON", responseString);
                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                                Log.i("JSON", response.toString());
                                Log.i("JSON", "Length" + response.length());
                                Log.i("JSON", "Date successfully updated");
                            }
                        });
            }
        };

        //Code for knowing the google username i.e if the user signs up via google
        if (haveNetworkConnection()) {

            toggle_switch = n.findViewById(R.id.toggle1);

            getStatus();

            toggle_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        Toast.makeText(getApplicationContext(), "Status Online", Toast.LENGTH_SHORT).show();
                        toggle_switch.setText("Account Status  ");
                        updateState(true);
                    } else {
                        Toast.makeText(getApplicationContext(), "Status Offline", Toast.LENGTH_SHORT).show();
                        toggle_switch.setText("Account Status  ");
                        updateState(false);
                    }
                }
            });


            toggle_switch.setTypeface(ResourcesCompat.getFont(getApplicationContext(), R.font.montserrat));

            count();
        } else {
            noNetworkAction();
        }

        Button updatebutton = findViewById(R.id.update_button);
        updatebutton.setHeight(update.getHeight());



    }

    private void updatedialog() {
        // Creates instance of the manager.
        appUpdateManager = AppUpdateManagerFactory.create(HomeScreen.this);

// Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

// Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(new OnSuccessListener<AppUpdateInfo>() {
            @Override
            public void onSuccess(AppUpdateInfo appUpdateInfo) {
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                        && appUpdateInfo.isUpdateTypeAllowed(IMMEDIATE)) {
                    try {
                        appUpdateManager.startUpdateFlowForResult(
                                // Pass the intent that is returned by 'getAppUpdateInfo()'.
                                appUpdateInfo,
                                // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                                IMMEDIATE,
                                // The current activity making the update request.
                                HomeScreen.this,
                                // Include a request code to later monitor this update request.
                                101);
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(HomeScreen.this, "update", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 101) {
            if (resultCode != RESULT_OK) {

                Log.i("Update flow failed!", "" + resultCode);
                // If the update is cancelled or fails,
                // you can request to start the update again.
            }
        }
    }


    //Used for getting the current user status
    private void getStatus() {

        Log.i("JSON", "Reached getStatus");

        RequestParams params = new RequestParams();
        params.put("email", email);

        AsyncHttpClient client = new AsyncHttpClient();

        String lddupdateurl = "https://bloodtransfer.herokuapp.com/index.php/data/getuserstatus";

        client.post(lddupdateurl, params, new

                JsonHttpResponseHandler() {

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject
                            errorResponse) {
                        Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                        Log.i("JSON", "Status code" + statusCode);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable
                            throwable) {
                        Log.i("JSON", "Status  code" + statusCode);
                        Log.i("JSON", responseString);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        Log.i("JSON", response.toString());

                        try {

                            if (response.getJSONObject(0).getString("userstatus").equals("0")) {
                                toggle_switch.setChecked(false);
                            } else {
                                toggle_switch.setChecked(true);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }


    //Backbutton overrided
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                finishAffinity();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //Navigation view
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {

            case R.id.profile:
                Intent inte = new Intent(getApplicationContext(), Profile.class);
                startActivity(inte);
                break;
            case R.id.Make_Requests:
                Intent don = new Intent(getApplicationContext(), BecomeDonor.class);
                startActivity(don);
                break;

            case R.id.raise:
                Intent raised = new Intent(getApplicationContext(), RaisedRequest.class);
                raised.putExtra("email", email);
                startActivity(raised);
                break;

            case R.id.Become_donor:
                Intent intent = new Intent(getApplicationContext(), RequestActivity.class);
                startActivity(intent);
                break;


            case R.id.about:
                break;
            case R.id.rateus:
                Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                }
                break;
            case R.id.feed:
                Intent intent1 = new Intent(getApplicationContext(), FeedBack.class);
                startActivity(intent1);
                break;
            case R.id.hos:
                Intent intent2 = new Intent(getApplicationContext(), HospitalsBloodBanks.class);
                startActivity(intent2);
                break;

            case R.id.instruction:
                Intent instruction = new Intent(getApplicationContext(), Instruction.class);
                startActivity(instruction);
                break;


            case R.id.privacy:
                Intent privacy = new Intent(getApplicationContext(), PrivacyPolicy.class);
                startActivity(privacy);
                break;



            case R.id.logout:
                Toast.makeText(getApplicationContext(), "Logged Out", Toast.LENGTH_SHORT).show();
                GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
                if (account != null) {
                    GoogleSignInOptions gso = new GoogleSignInOptions.
                            Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
                            build();

                    GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(getApplicationContext(), gso);
                    googleSignInClient.signOut();
                } else {
                    LoginManager.getInstance().logOut();
                }
                Intent logout = new Intent(getApplicationContext(), LoginScreen.class);
                logout.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(logout);

                break;

            default:
                return true;
        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    //Used for updating the user status
    private void updateState(boolean bool) {
        String stateurl = "https://bloodtransfer.herokuapp.com/index.php/data/updatestatus";

        RequestParams params = new RequestParams();

        Log.i("JSON", bool + "update");
        if (bool) {
            params.put("status", 1);
        } else {
            params.put("status", 0);
        }

        params.put("email", email);

        AsyncHttpClient client = new AsyncHttpClient();

        client.post(stateurl, params, new

                JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Log.i("JSON", "JSON is " + response.toString());
                        Log.i("JSON", "In UpdateState()");
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject
                            errorResponse) {
                        Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                        Log.i("JSON", "Status code" + statusCode);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable
                            throwable) {
                        Log.i("JSON", "Status  code" + statusCode);
                        Log.i("JSON", responseString);
                        Log.i("JSON", "Update method");
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        Log.i("JSON", response.toString());
                        Log.i("JSON", "Length" + response.length());
                        Log.i("JSON", "In UpdateState()");
                    }
                });
    }


    //Used for fetching the information of the user if he is signed in using facebook
    private void getFbInfo() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();

        if (isLoggedIn) {
            GraphRequest request = GraphRequest.newMeRequest(
                    AccessToken.getCurrentAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            try {
//                            Log.d(LOG_TAG, "fb json object: " + object);
//                            Log.d(LOG_TAG, "fb graph response: " + response);

                                String first_name = object.getString("first_name");
                                String last_name = object.getString("last_name");
//                            String gender = object.getString("gender");
//                            String birthday = object.getString("birthday");
//                            String image_url = "http://graph.facebook.com/" + id + "/picture?type=large";

                                if (first_name != null && last_name != null) {
                                    usrnm.setText(String.format("%s %s", first_name, last_name));
                                    propic.setText(String.valueOf(first_name.charAt(0)));
                                }

                                if (object.has("email")) {
                                    email = object.getString("email");
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "error in name", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,first_name,last_name,email,gender,birthday"); // id,first_name,last_name,email,gender,birthday,cover,picture.type(large)
            request.setParameters(parameters);
            request.executeAsync();
        }
    }


    //Used for fetching the donor and request count from the database
    private void count() {
        String don_url = "https://bloodtransfer.herokuapp.com/index.php/data/donorcount";
        String rec_url = "https://bloodtransfer.herokuapp.com/index.php/data/reqcount";
        AsyncHttpClient client = new AsyncHttpClient();

        client.post(don_url, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i("JSON", "JSON is " + response.toString());
                Log.i("JSON", response.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.i("JSON", "Status code" + statusCode);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "Request Success:)", Toast.LENGTH_SHORT).show();
                Log.i("JSON", responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                Log.i("JSON", response.toString());
                TextView don_count;
                don_count = findViewById(R.id.don_count_home);

                try {
                    JSONObject obj = (JSONObject) response.get(0);
                    don_count.setText(obj.getString("count(*)"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        client.post(rec_url, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i("JSON", "JSON is " + response.toString());
                Log.i("JSON", response.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.i("JSON", "Status code" + statusCode);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "Request Success:)", Toast.LENGTH_SHORT).show();
                Log.i("JSON", responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                Log.i("JSON", response.toString());
                TextView don_count2;
                don_count2 = findViewById(R.id.don_count2_home);

                try {
                    JSONObject obj = (JSONObject) response.get(0);
                    don_count2.setText(obj.getString("count(*)"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    //Used for checking whether the device is connected to the internet or not
    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    private void noNetworkAction() {
        final Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                "No internet connection.",
                Snackbar.LENGTH_INDEFINITE);
        snackbar.setActionTextColor(ContextCompat.getColor(Objects.requireNonNull(getApplicationContext()),
                R.color.black));
        snackbar.setAction(R.string.try_again, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //recheck internet connection and call DownloadJson if there is internet
                if (!haveNetworkConnection())
                    noNetworkAction();
                else {
                    Intent intent = getIntent();
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    finish();
                    startActivity(intent);
                }
            }
        }).show();
    }



    private void showAlert(){
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.layout_custom_dialog, null);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);

        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(true);

        Button update = alertLayout.findViewById(R.id.update);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int date = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(HomeScreen.this, android.R.style.Theme_Holo_Dialog_NoActionBar_MinWidth, dateSetListener, year, month, date);
                Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        dateSetListener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;

                String dat = year + "/" + month + "/" + dayOfMonth;

                RequestParams params = new RequestParams();
                params.put("email", email);
                params.put("lastdonated", dat);


                AsyncHttpClient client = new AsyncHttpClient();

                String lddupdateurl = "https://bloodtransfer.herokuapp.com/index.php/data/lastdonatedupdate";

                client.post(lddupdateurl, params, new

                        JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                Log.i("JSON", "JSON is " + response.toString());
                                Log.i("JSON", "Status  code" + statusCode);
                                Log.i("JSON", response.toString());
                                Toast.makeText(getApplicationContext(),"Date Updated",Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject
                                    errorResponse) {
                                Toast.makeText(getApplicationContext(), "Request Failed:(", Toast.LENGTH_SHORT).show();
                                Log.i("JSON", "Status code" + statusCode);
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable
                                    throwable) {
                                Log.i("JSON", "Status  code" + statusCode);
                                Log.i("JSON", responseString);
                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                                Log.i("JSON", response.toString());
                                Log.i("JSON", "Length" + response.length());
                                Log.i("JSON", "Date successfully updated");
                                Toast.makeText(getApplicationContext(),"Date Updated",Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        };


        AlertDialog dialog = alert.create();
        dialog.show();

    }


    //On resume code for update of app
//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        appUpdateManager
//                .getAppUpdateInfo()
//                .addOnSuccessListener(
//                        new OnSuccessListener<AppUpdateInfo>() {
//                            @Override
//                            public void onSuccess(AppUpdateInfo appUpdateInfo) {
//                                if (appUpdateInfo.updateAvailability()
//                                        == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
//                                    // If an in-app update is already running, resume the update.
//                                    try {
//                                        appUpdateManager.startUpdateFlowForResult(
//                                                appUpdateInfo,
//                                                IMMEDIATE,
//                                                HomeScreen.this,
//                                                101);
//                                    } catch (IntentSender.SendIntentException e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//                            }
//                        });
//    }
}
